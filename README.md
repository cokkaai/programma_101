# programma 101

## What is it?

A simulator for the Olivetti P101.

## Why?

This ancient machine is not very popular. While some simulator exists, I decided to write my own to experiment with simulator design and Rust.

## How? 

When I started I had no idea how a P101 works. As a precursor of modern computers, It has a particular programming language and method; interacting with it also sound strange.
So I just started with the generic idea of a simulator driven by events. Things happen and propagate to interested parties, which may react and emit further events.
The project is split in different crates to make it modular.  
- The [p101_is -instruction set-](<https://gitlab.com/cokkaai/programma_101/-/tree/master/is>) crate is a general library to express a P101 program in a type safe way. Moreover it defines encodings to convert the program to different representations  
- The [c101](<ihttps://gitlab.com/cokkaai/programma_101/-/tree/master/c101>) crate is for converting a program between some of the syntax and encodings you may found in the Internet. There is not a shared standard and I developed encoders and decoders to convert programs to and from my own format, in order to promote interoperability  
- The [p101_sys](<https://gitlab.com/cokkaai/programma_101/-/tree/master/sys>) crate implements the event bus and components. That is the nuts and bolts to build a simulator  
- The [p101_term] crate implements a simple simulator running in a terminal.

## Now?

The core simulator is almost complete but implemented devices are test-mocks or simple command-line I/O components.
Calculator accuracy should be quite accurate. User experience is still a work in progress.

## Next?

My plan are building a full web GUI based on react, which I want lo learn to a certain degree.
Logic will be compiled to WASM.
Maybe I could implement the simulator as an Internet API to make it easily usable from third parties.