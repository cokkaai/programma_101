# Managing script formats with C101

C101 is the Programma 101 format conversion utility. In the Unix mindset, it allows to combine different filters in pipelines to apply different trasformations.

## Pipelines

The conversion of a file to a new format is made of different stages. Each stage is a filter which trasforms its input in another form. Executing many filters in sequence, all required encodings can be managed: this is the concept of pipeline.

Each filter has specific input and output and must be properly combined. A stage output format must match the next stage input format. For instance, to read the prg.101 file from disk, decode from the Larini format, encode it in standard UTF8 and write the program to the standard out one would write: 

c101 -f read_file -f larini_decoder -f standard_encoder -f write_to_stdout prg.101

The last parameter is always the file to process. 
While it may seems complicated, it allows to add a new formats by implementing the specific business logic in a new filter.

## Filters

In order to build your own conversione pipeline, a short reference follows

- [read_file] - Read the input file from disk into memory as raw bytes.
- [utf8_decoder] - Convert bytes into UTF-8. The conversion may fail.
- [standard_decoder] - Convert UTF-8 data into P101 program.
- [larini_decoder] - Convert a P101 program from the Larini format to the standard one.
- [standard_encoder] - Encode a P101 program to UTF-8 using the standard encoding
- [write_to_stdout] - Write its input to the standard output

## Creating a new filter

A filter, by the trait definition, maps an [EncodingData]  value to another.
It means that you can transform raw data to a file or text or program a viceversa.
Each filter accepts only the EncodingData it can process; for instance the read_file filter refues any input that is not an EncodingData::File value.

[read_file]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/c101/src/filter/read_file.rs>
[utf8_decoder]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/c101/src/filter/utf8_decoder.rs>
[standard_decoder]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/c101/src/filter/standard_decoder.rs>
[larini_decoder]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/c101/src/filter/larini_decoder.rs>
[standard_encoder]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/c101/src/filter/standard_encoder.rs>
[write_to_stdout]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/c101/src/filter/write_to_stdout.rs>
[EncodingData]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/c101/src/filter.rs>


