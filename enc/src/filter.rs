mod standard_decoder;
mod larini_decoder;
mod utf8_decoder;
mod standard_encoder;
mod read_file;
mod write_to_stdout;

use p101_is::program::*;

pub use standard_decoder::*;
pub use larini_decoder::*;
pub use utf8_decoder::*;
pub use standard_encoder::*;
pub use read_file::*;
pub use write_to_stdout::*;

#[derive(Clone, Debug)]
pub enum EncodingData {
    File(String),
    Raw(Vec<u8>),
    Text(String),
    Program(Program),
}

pub trait Filter {
    fn name(&self) -> &str;
    fn map(&self, data: EncodingData) -> Result<EncodingData, String>;
}
