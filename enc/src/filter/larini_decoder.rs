use crate::filter::{EncodingData, Filter};

pub struct LariniDecoder;

impl LariniDecoder {
    pub const NAME: &'static str = "larini_decoder";

    pub fn new() -> Self {
        Self
    }
    
    fn validate(&self, data: EncodingData) -> Result<Vec<u8>, String> {
        match data {
            EncodingData::Raw(bytes) => Ok(bytes),
            _ => Err(format!("{}: need a text input", Self::NAME)),
        }
    }

    fn decode(&self, data: Vec<u8>) -> Result<EncodingData, String> {
        let text = p101_is::decoder::CodePage437::new()
            .map(&data);

        p101_is::decoder::LariniDecoder::new()
            .decode(&text)
            .map(EncodingData::Program)
    }
}

impl Default for LariniDecoder {
    fn default() -> Self {
        Self::new()
    }
}

impl Filter for LariniDecoder {
    fn name(&self) -> &str {
        Self::NAME
    }

    fn map(&self, data: EncodingData) -> Result<EncodingData, String> {
        self.validate(data)
            .and_then(|text| self.decode(text))
    }
}
