use super::{EncodingData, Filter};

pub struct ReadFile;

impl ReadFile {
    pub const NAME: &'static str = "read_file";

    pub fn new() -> Self {
        Self
    }
    
    fn validate(&self, data: EncodingData) -> Result<String, String> {
        match data {
            EncodingData::File(path) => Ok(path),
            _ => Err(format!("{}: need a file input", Self::NAME)),
        }
    }

    fn read(&self, path: String) -> Result<EncodingData, String> {
        match std::fs::read(&path) {
            Ok(data) => {
                Ok(EncodingData::Raw(data))
            },
            Err(e) => {
                Err(format!("{}: Cannot read file {}: {}", Self::NAME, &path, e))
            }
        }
    }
}

impl Default for ReadFile {
    fn default() -> Self {
        Self::new()
    }
}

impl Filter for ReadFile {
    fn name(&self) -> &str {
        Self::NAME
    }

    fn map(&self, data: EncodingData) -> Result<EncodingData, String> {
        self.validate(data)
            .and_then(|path| self.read(path))
    }
}
