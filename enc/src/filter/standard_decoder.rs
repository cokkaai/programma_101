use super::{EncodingData, Filter};

use p101_is::encoding::*;
use p101_is::decoder::*;

pub struct StandardDecoder;

impl StandardDecoder {
    pub const NAME: &'static str = "standard_decoder";

    pub fn new() -> Self {
        Self
    }
    
    fn validate(&self, data: EncodingData) -> Result<String, String> {
        match data {
            EncodingData::Text(text) => Ok(text),
            _ => Err(format!("{}: need a text input", Self::NAME)),
        }
    }

    fn decode(&self, text: String) -> Result<EncodingData, String> {
        match Decoder::<StandardEncoding>::new().decode(&text) {
            Ok(p) => Ok(EncodingData::Program(p)),
            Err(e) => Err(e),
        }
    }
}

impl Default for StandardDecoder {
    fn default() -> Self {
        Self::new()
    }
}

impl Filter for StandardDecoder {
    fn name(&self) -> &str {
        Self::NAME
    }

    fn map(&self, data: EncodingData) -> Result<EncodingData, String> {
        self.validate(data)
            .and_then(|text| self.decode(text))
    }
}
