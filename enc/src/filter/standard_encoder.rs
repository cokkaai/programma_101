use super::{EncodingData, Filter};
use p101_is::encoding::*;
use p101_is::encoder::*;
use p101_is::program::*;

pub struct StandardEncoder;

impl StandardEncoder {
    pub const NAME: &'static str = "standard_encoder";

    pub fn new() -> Self {
        Self
    }

    fn validate(&self, data: EncodingData) -> Result<Program, String> {
        match data {
            EncodingData::Program(p) => {
                Ok(p)
            },
            _ => Err(format!("{}: need a program input", Self::NAME))
        }
    }

    fn program_to_text(&self, data: Program) -> Result<EncodingData, String> {
        let enc = Encoder::<StandardEncoding, NullAnnotator>::new();
        let text = enc.encode(&data);
        Ok(EncodingData::Text(text))
    }
}

impl Default for StandardEncoder {
    fn default() -> Self {
        Self::new()
    }
}

impl Filter for StandardEncoder {
    fn name(&self) -> &str {
        Self::NAME
    }

    fn map(&self, data: EncodingData) -> Result<EncodingData, String> {
        self.validate(data)
            .and_then(|program| self.program_to_text(program))
    }
}
