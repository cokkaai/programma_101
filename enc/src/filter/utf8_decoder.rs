use crate::filter::{EncodingData, Filter};

pub struct Utf8Decoder;

impl Utf8Decoder {
    pub const NAME: &'static str = "utf8_decoder";

    pub fn new() -> Self {
        Self
    }
    
    fn validate(&self, data: EncodingData) -> Result<String, String> {
        match data {
            EncodingData::Raw(bytes) => {
                String::from_utf8(bytes)
                    .map_err(|e| format!("{}", e))
            },
            _ => Err(format!("{}: need a raw input", Self::NAME)),
        }
    }

    fn decode(&self, text: String) -> Result<EncodingData, String> {
        Ok(EncodingData::Text(text))
    }
}

impl Default for Utf8Decoder {
    fn default() -> Self {
        Self::new()
    }
}

impl Filter for Utf8Decoder {
    fn name(&self) -> &str {
        Self::NAME
    }

    fn map(&self, data: EncodingData) -> Result<EncodingData, String> {
        self.validate(data)
            .and_then(|text| self.decode(text))
    }
}
