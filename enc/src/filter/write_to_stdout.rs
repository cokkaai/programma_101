use super::{EncodingData, Filter};

pub struct WriteToStdout;

impl WriteToStdout {
    pub const NAME: &'static str = "write_to_stdout";

    pub fn new() -> Self {
        Self
    }
    
    fn validate(&self, data: EncodingData) -> Result<String, String> {
        match data {
            EncodingData::Text(text) => {
                Ok(text)
            },
            _ => Err(format!("{}: need a text input", Self::NAME)),            
        }
    }

    fn write(text: String) -> Result<EncodingData, String> {
        println!("{}", text);
        Ok(EncodingData::Text(text))
    }
}

impl Default for WriteToStdout {
    fn default() -> Self {
        Self::new()
    }
}

impl Filter for WriteToStdout {
    fn name(&self) -> &str {
        Self::NAME
    }

    fn map(&self, data: EncodingData) -> Result<EncodingData, String> {
        self.validate(data)
            .and_then(Self::write)
    }
}
