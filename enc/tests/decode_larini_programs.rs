use p101_enc::filter::*;
use p101_enc::pipeline::*;
use p101_is::instruction::*;

#[test]
pub fn convert_and_trim_program_136() {
    // setup c101 to convert program as in 
    // c101 p101card.136 -i larini_encoding
    const FILE: &'static str= "tests/p101card.136";

    let stages: Vec<Stage> = vec!(
        Stage::new(Box::new(ReadFile::new()), false),
        Stage::new(Box::new(LariniDecoder::new()), true));

    let options = PipelineOptions {
        display_messages: false,
        display_errors: false,
    };

    let pipeline = Pipeline::new(stages, options);

    let r = pipeline.run(EncodingData::File(FILE.into()));

    assert_eq!(true, r.is_ok());

    let output = r.unwrap();
    assert_eq!(1, output.inspections.len());

    match &output.inspections[0] {
        EncodingData::Program(p) => {
            assert_eq!(Instruction::Label(JumpDestination::AV), p[0]);
            assert_eq!(Instruction::Stop, p[1]);
            assert_eq!(Instruction::CopyM(Operand::B), p[2]);
            assert_eq!(Instruction::CopyM(Operand::C), p[3]);
            assert_eq!(Instruction::Label(JumpDestination::BV), p[4]);
            assert_eq!(Instruction::CopyToA(Operand::C), p[5]);
            assert_eq!(Instruction::Mul(Operand::B), p[6]);
            assert_eq!(Instruction::Print(Operand::A), p[7]);
            assert_eq!(Instruction::SwapA(Operand::B), p[8]);
            assert_eq!(Instruction::Jump(Origin::CV), p[9]);
        },
        _ => assert!(false),
    };
}

#[test]
pub fn convert_and_trim_program_138() {
    // setup c101 to convert program as in 
    // c101 p101card.136 -i larini_encoding
    const FILE: &'static str= "tests/p101card.138";

    let stages: Vec<Stage> = vec!(
        Stage::new(Box::new(ReadFile::new()), false),
        Stage::new(Box::new(LariniDecoder::new()), true));

    let options = PipelineOptions {
        display_messages: false,
        display_errors: false,
    };

    let pipeline = Pipeline::new(stages, options);

    let r = pipeline.run(EncodingData::File(FILE.into()));

    assert_eq!(true, r.is_ok());

    let output = r.unwrap();
    assert!(output.inspections.len() > 0);
}
