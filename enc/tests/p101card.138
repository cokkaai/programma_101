' queens.p101: Solving the (8-)queens problem using an Olivetti P101.
' 2013-Nov-04 20.15 / TN
'
' Created to maintain the code in machine readable form, but also with
' an eye to eventually, perhaps, write a simulator.
'
' Each P101 instruction on a separate line, at most the two first words
' are significant, the rest if the line is ignored. ' in a line and the
' rest of the line is ignored. Empty lines are ignored.
'
' Use of registers:
'
'   Register  Use         Initial value
'   --------  ----------------------------------------  ----------------
'   b         Debug: Descreased by 0.1 regularly,       Set to fit
'             program stops whenever result found to    debugging needs
'             be <= 0.                                  (try 10000)
'   B         n/10 where n <= 9 is the board size       n/10
'   c         j/10: j steps through columns i-1, ... 1  None needed
'             when checking queen in column i
'   C         Board/10^(n-i+1): One digit per column,   0
'             the value gives the row (0, 1, ... n-1)
'             of the queen. The value may be referred
'             to as row[1]row[2]...row[i-1].row[i] or
'             simply row[1..i].
'   d         row[1..j] during checking.                None needed
'   D         Constant 0.1                              0.1
'   e         i/10 where i is current column            0
'   E         row[i] during checking                    None needed
'
' Operating instructions:
'
'   1. Enter the program.
'   2. Set decimal wheel at 1.
'   3. Enter suitable debugging value into register b and the board
'      size, divided by 10, into register B.
'   4. Enter the values indicated into registers C, D, and e.
'   5. Press V.
'
' Solutions will be presented as a direct print of register C. For
' example, the first few solutions to the 8-queens problem will be
' printed as:
'
'   475261.3 C Diamond
'   572631.4 C Diamond
'   ...
'
' When interpreting this, it is important to add a leading zero, not
' printed, for the earlier solutions: For the 8-queen problem, 8
' digits must be understood. Thus, the above numbers represent
'
'   x . . . . . . .
'   . . . . . . x .
'   . . . . x . . .
'   . . . . . . . x
'   . x . . . . . .
'   . . . x . . . .
'   . . . . . x . .
'   . . x . . . . .
'
' and
'
'   x . . . . . . .
'   . . . . . . x .
'   . . . x . . . .
'   . . . . . x . .
'   . . . . . . . x
'   . x . . . . . .
'   . . . . x . . .
'   . . x . . . . .
'
'
' For another example, all the solutions to the 4-queens problem are
' printed as:
'
'   130.2 C
'   203.1 C
'
'
AV   'next column:
'
B/  'Debugging sequence:
D-   '  b - 0.1
B/  '  -> b
D/Y  '  if ( earlier b <= 0 ) {
S    '    STOP
E/Y  '}
'
B   'n/10
E/-  '- i/10
C/V  'if ( i >= n ) {
C   '    print board
DV   'goto next row
B/V  '}
'
C   'Board
D�   '* 10
C   '-> Board (make room for row[i+1])
'
E/  'i/10
D+   '+ 1/10
E/  'i = i + 1
'
EY   'check column:
'
C   '  Board
/   '  M = decimal part of Board (= row[i])
E   '  Store row[i]
-    '  Detach row[i]
D/  '  Store row[1..i-1]
'
B   '  n/10
E-   '  - row[i]/10
C/Z  '  if ( row[i] >= n ) { previous column:
D/  '    row[1..i-1]
Dx   '    row[1..i-1]/10
C   '    -> Board
E/  '    i/10
D-   '    - 1/10
E/  '    i = i - 1
E/  '    i/10
D/V  '    if ( i > 0 ) goto next row (was: check column)
AW   '    while ( true ) {
S    '      STOP
W    '    }
B/Z  '  }
'
E/+  '  j = i (into M)
'
E/W  '  next j:
    '  j into A
D-   '  j - 1
C/W  '  if ( j <= 0 ) {
V    '    goto next column
B/W  '  }
C/  '  j = j - 1
D/  '  row[1..j]
Dx   '  /10
/   '  M = decimal part of row[1..j] = row[j]
-    '  Detach row[j]
D/  '  Store row[1..j-1]
    '  row[j] into A
E-   '  row[j] - row[i]
A   '  abs( row[j] - row[i] )
C/Y  '  if ( abs( row[j] - row[i] ) == 0 ) {
DV   '    goto next row
B/Y  '  }
E/-  '  - i/10
C/+  '  + j/10 (into M)
A   '  abs( abs( row[j] - row[i] ) - i + j )
'
D/W  '  if ( abs( abs( row[j] - row[i] ) - i + j ) != 0 ) goto next j
'
EV   'next row:
E/V  '  ...
'       
C   '  Board
D+   '  + 1/10
C   '  -> Board (row[i]++)
'
DY   '  goto check column
' #0
' 0
' 0.1
' 0
' 0
' 0
' 0

