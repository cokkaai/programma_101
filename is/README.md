A simulator, as any computer, it is hardly useful with no programs. Since I didn't know how to program a P101, I started looking at other simulators for P101 sources.
I found out that there is no standard format. So, in order to read all such formats I implemented a generic framework leveraging traits and a strongly typed instruction representation.

The [Encoding] trait defines how to convert a text line in an instruction and vice versa.
There are two encodings:
- the [standard] one which represent instructions using the original code symbols. Once you master the programming language it is easy to read. On the other hand, writing code is not straightforward because many symbols are not available on modern keyboards; you have to copy paste the exact unicode character.
- the second one is the [AsciiEncoding], which takes the opposite approach and express operations using tokens made of ASCII only characters. Easier to write but less readable in my own opinion. Decoding is not implemented as of now.

On the web I found other encoding loosely based on CSV triples that I may support in the future.
For instance [https://github.com/Fabioamd87/P101], which is similar to the (retired?) emulator for University of Cassino.

Optionally one can implement the Annotator trait to comment each line during encoding. I find this improve code readability as P101 instruction are largely simbolic and you have to get used to.

Encodings are used by a generic [Encoder] to convert program into text and a generic [Decoder] to convert a text into a Program.
The [LariniDecoder] is specialized for the format used by the [simulator wrote by Claudio Larini] that also has made available most P101 programs.

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[is]: <https://gitlab.com/cokkaai/programma_101/-/tree/master/is>
[Encoding]:  <https://gitlab.com/cokkaai/programma_101/-/blob/master/is/src/encoding.rs>
[standard]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/is/src/encoding/standard_encoding.rs>
[AsciiEncoding]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/is/src/encoding/ascii_encoding.rs>
[https://github.com/Fabioamd87/P101]: <https://github.com/Fabioamd87/P101>
[Encoder]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/is/src/encoder/encoder.rs>
[Decoder]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/is/src/decoder/decoder.rs>
[LariniDecoder]: <https://gitlab.com/cokkaai/programma_101/-/blob/master/is/src/decoder/larini_decoder.rs>
[simulator wrote by Claudio Larini]: <http://www.claudiolarini.altervista.org/emul2.htm>
