mod standard_encoding;
mod ascii_encoding;

pub use standard_encoding::*;
pub use ascii_encoding::*;

use crate::instruction::*;

pub enum DecodeResult {
    /// Wraps the decoded instruction
    Ok(Instruction),

    /// Input string contains no instruction
    None,

    /// Error decoding input string
    Err(String),
}

/// Defines methods required to encode all supported instructions
pub trait Encoding {
    fn encode(&self, i: &Instruction) -> String;
    
    fn decode_instr(&self, text: &str) -> DecodeResult;

    fn decode_cai(&self, text: &str) -> DecodeResult;
}
