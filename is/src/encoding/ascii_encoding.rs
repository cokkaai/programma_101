use crate::instruction::*;
use crate::encoding::*;

#[derive(Default)]
/// AsciiEncoding is an alternative to StandardEncoding.
/// It does not use original P101 symbols to express
/// instructions in ASCII.
pub struct AsciiEncoding;

impl AsciiEncoding {
    const OP_ADD: &'static str = "add";
    const OP_COPY_M: &'static str = "copy_m";
    const OP_COPY_TO_A: &'static str = "copy_to_a";
    const OP_DIV: &'static str = "div";
    const OP_MUL: &'static str = "mul";
    const OP_PRINT: &'static str = "print";
    const OP_RESET: &'static str = "reset";
    const OP_SUB: &'static str = "sub";
    const OP_SWAP: &'static str = "swap";
    const OP_CAI: &'static str = "cai";

    fn add(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_ADD.into(),
            _ => format!("{} {}", o, Self::OP_ADD)
        }        
    }
    
    fn copy_m(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_COPY_M.into(),
            _ => format!("{} {}", o, Self::OP_COPY_M)
        }        
    }
    
    fn copy_to_a(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_COPY_TO_A.into(),
            _ => format!("{} {}", o, Self::OP_COPY_TO_A)
        }        
    }

    fn div(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_DIV.into(),
            _ => format!("{} {}", o, Self::OP_DIV)
        }
    }
    
    fn mul(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_MUL.into(),
            _ => format!("{} {}", o, Self::OP_MUL)
        }        
    }
    
    fn print(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_PRINT.into(),
            _ => format!("{} {}", o, Self::OP_PRINT)
        }        
    }

    fn reset(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_RESET.into(),
            _ => format!("{} {}", o, Self::OP_RESET)
        }
    }
   
    fn sub(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_SUB.into(),
            _ => format!("{} {}", o, Self::OP_SUB)
        }
    }

    fn swap(&self, o: &Operand) -> String {
        match o {
            Operand::M => Self::OP_SWAP.into(),
            _ => format!("{} {}", o, Self::OP_SWAP)
        }
    }

    fn cai(&self, sign: &CaiSign, order: &CaiOrder, digit: &CaiDigit, comma: &CaiComma) -> std::string::String {
        format!("{}{}{}{} {}", order, sign, digit, comma, Self::OP_CAI)
    }
}

impl Encoding for AsciiEncoding {
    fn encode(&self, i: &Instruction) -> String {
        match i {
            Instruction::Abs => "A abs".into(),
            Instruction::Add(o) => self.add(o),
            Instruction::CaiStart => "cai".into(),
            Instruction::Cai(sign, order, digit, comma) => format!("{}{}{}{} cai", order, sign, digit, comma),
            Instruction::ConditionalJump(o) => o.to_string(),
            Instruction::CopyDecimal => "decimals".into(),
            Instruction::CopyM(o) => self.copy_m(o),
            Instruction::CopyToA(o) => self.copy_to_a(o),
            Instruction::Div(o) => self.div(o),
            Instruction::DrExchange => "dr_exchange".into(),
            Instruction::Jump(o) => o.to_string(),
            Instruction::Label(d) => d.to_string(),
            Instruction::Mul(o) => self.mul(o),
            Instruction::NewLine => "newline".into(),
            Instruction::Print(o) => self.print(o),
            Instruction::Reset(o) => self.reset(o),
            Instruction::Sqr(o) => format!("{} sqr", o),
            Instruction::Stop => "stop".into(),
            Instruction::Sub(o) => self.sub(o),
            Instruction::SwapA(o) => self.swap(o),
        }
    }

    fn decode_instr(&self, _text: &str) -> DecodeResult {
        unimplemented!()

        // Instructions always 1 character long
        // S stop

        // Instructions always 2 character long
        // A↕ abs
        // /↕ copy decimals
        // /⋄ Newline
        // RS DRexchange

        // Instructions that can range from 1 to 3 charaters long
        // [reg[/]](+|-|x|÷|√)
        // [reg[/]]↑ copy m
        // [reg[/]]↓ copy to a
        // [reg[/]]↕ swap
        // [reg[/]]* reset

        // Instructions that can range be 1 or 2 charaters long
        // (.*)V|W|Y|Z unconditional jump is 1 or 2 characters long ending with 
        // [.*]V|W|Y|Z conditional jump is always 2 characters long
        // [.*]V|W|Y|Z jump destinantion is always 2 characters long
        
        // CAI expressions needs a separate parsing function
        // because uses the same expressions as some instructions.
        // The only recognized instruction is CAI start A/↑
    }

    fn decode_cai(&self, _text: &str) -> DecodeResult {
        unimplemented!()
    }
}

#[cfg(test)]
mod tests {
    use crate::instruction::*;
    use crate::program::Program;
    use crate::encoding::*;
    use crate::encoder::*;
    
    fn build_encoder() -> Encoder<AsciiEncoding, NullAnnotator> {
        Encoder::<AsciiEncoding, NullAnnotator>::new()
    }

    #[test]
    pub fn ascii_encoding_of_stop_is_s() {
        let program: Program = vec!(Instruction::Stop);
        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(1, lines.len());
        assert_eq!("stop", lines[0]);
    }

    #[test]
    pub fn ascii_encoding_of_reset_is_reg_and_asterisk() {
        let program: Program = vec!(Instruction::Reset(Operand::A),
            Instruction::Reset(Operand::B),
            Instruction::Reset(Operand::b),
            Instruction::Reset(Operand::M));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(4, lines.len());
        assert_eq!(String::from("A reset"), lines[0]);
        assert_eq!(String::from("B reset"), lines[1]);
        assert_eq!(String::from("B/ reset"), lines[2]);
        assert_eq!(String::from("reset"), lines[3]);
    }

    #[test]
    pub fn ascii_encoding_of_print_is_reg_and_diamond() {
        let program: Program = vec!(Instruction::Print(Operand::A),
            Instruction::Print(Operand::B),
            Instruction::Print(Operand::b),
            Instruction::Print(Operand::M));

        let encoder = build_encoder();

        let text = encoder.encode(&program);

        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(4, lines.len());

        assert_eq!(String::from("A print"), lines[0]);
        assert_eq!(String::from("B print"), lines[1]);
        assert_eq!(String::from("B/ print"), lines[2]);
        assert_eq!(String::from("print"), lines[3]);
    }

    #[test]
    pub fn ascii_encoding_of_newline_is_slash_diamond() {
        let program: Program = vec!(Instruction::NewLine);
        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(1, lines.len());
        assert_eq!(String::from("newline"), lines[0]);
    }

    #[test]
    pub fn ascii_encoding_of_copy_to_a_is_reg_and_down_arrow() {
        let program: Program = vec!(Instruction::CopyToA(Operand::M),
            Instruction::CopyToA(Operand::B),
            Instruction::CopyToA(Operand::b));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!(String::from("copy_to_a"), lines[0]);
        assert_eq!(String::from("B copy_to_a"), lines[1]);
        assert_eq!(String::from("B/ copy_to_a"), lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_copy_m_is_reg_and_up_arrow() {
        let program: Program = vec!(Instruction::CopyM(Operand::M),
            Instruction::CopyM(Operand::B),
            Instruction::CopyM(Operand::b));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!(String::from("copy_m"), lines[0]);
        assert_eq!(String::from("B copy_m"), lines[1]);
        assert_eq!(String::from("B/ copy_m"), lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_exchange_is_reg_and_up_down_arrow() {
        let program: Program = vec!(Instruction::SwapA(Operand::M),
            Instruction::SwapA(Operand::B),
            Instruction::SwapA(Operand::b));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!("swap", lines[0]);
        assert_eq!("B swap", lines[1]);
        assert_eq!("B/ swap", lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_d_r_exchange_is_rs() {
        let program: Program = vec!(Instruction::DrExchange);
        let encoder = build_encoder();

        let text = encoder.encode(&program);

        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(1, lines.len());
        assert_eq!("dr_exchange", lines[0]);
    }

    #[test]
    pub fn ascii_encoding_of_decimal_part_to_is_slash_up_down_arrow() {
        let program: Program = vec!(Instruction::CopyDecimal);

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(1, lines.len());
        assert_eq!("decimals", lines[0]);
    }

    #[test]
    pub fn ascii_encoding_of_add_is_reg_plus() {
        let program: Program = vec!(Instruction::Add(Operand::M),
            Instruction::Add(Operand::B),
            Instruction::Add(Operand::b));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!("add", lines[0]);
        assert_eq!("B add", lines[1]);
        assert_eq!("B/ add", lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_sub_is_reg_minus() {
        let program: Program = vec!(Instruction::Sub(Operand::M),
            Instruction::Sub(Operand::B),
            Instruction::Sub(Operand::b));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!("sub", lines[0]);
        assert_eq!("B sub", lines[1]);
        assert_eq!("B/ sub", lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_mul_is_reg_x() {
        let program: Program = vec!(Instruction::Mul(Operand::M),
            Instruction::Mul(Operand::B),
            Instruction::Mul(Operand::b));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!("mul", lines[0]);
        assert_eq!("B mul", lines[1]);
        assert_eq!("B/ mul", lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_div_is_reg_divided_by() {
        let program: Program = vec!(Instruction::Div(Operand::M),
            Instruction::Div(Operand::B),
            Instruction::Div(Operand::b));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!("div", lines[0]);
        assert_eq!("B div", lines[1]);
        assert_eq!("B/ div", lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_sqr() {
        // TODO
    }

    #[test]
    pub fn ascii_encoding_of_abs_is_accumulator_up_down_arrows() {
        let program: Program = vec!(Instruction::Abs);
        let encoder = build_encoder();

        let text = encoder.encode(&program);
        
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(String::from("A abs"), lines[0]);
    }

    #[test]
    pub fn ascii_encoding_of_jump_is_the_point_of_origin() {
        let program: Program = vec!(Instruction::Jump(Origin::V),
            Instruction::Jump(Origin::RZ),
            Instruction::Jump(Origin::CW));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!("V", lines[0]);
        assert_eq!(String::from("RZ"), lines[1]);
        assert_eq!(String::from("CW"), lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_conditional_jump_the_point_of_origin() {
        let program: Program = vec!(Instruction::ConditionalJump(ConditionalOrigin::_V),
            Instruction::ConditionalJump(ConditionalOrigin::rV),
            Instruction::ConditionalJump(ConditionalOrigin::dY));

        let encoder = build_encoder();

        let text = encoder.encode(&program);
        let lines: Vec<&str> = text.lines().collect();

        assert_eq!(3, lines.len());
        assert_eq!("/V", lines[0]);
        assert_eq!(String::from("rV"), lines[1]);
        assert_eq!(String::from("dY"), lines[2]);
    }

    #[test]
    pub fn ascii_encoding_of_cai() {
        // TODO
        return;
    }
}
