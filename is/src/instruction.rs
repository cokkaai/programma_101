use std::fmt;

#[allow(non_camel_case_types)]
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Operand {
    M,
    A,
    R,
    b,
    B,
    c,
    C,
    d,
    D,
    e,
    E,
    f,
    F,
}

impl fmt::Display  for Operand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let text = match self {
            Operand::A => "A",
            Operand::B => "B",
            Operand::b => "B/",
            Operand::C => "C",
            Operand::c => "C/",
            Operand::D => "D",
            Operand::d => "D/",
            Operand::E => "E",
            Operand::e => "E/",
            Operand::F => "F",
            Operand::f => "F/",
            Operand::M => "M",
            Operand::R => "R"
        };

        write!(f, "{}", text)
    }
}


/// Points of origin of unconditional jumps.
/// An abbreviated form of this list is to combine V, W, Y or Z as follows:
/// ∆  -> A∆
/// C∆ -> B∆
/// D∆ -> E∆
/// R∆ -> F∆
/// ∆ = V, W, Y or Z
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Origin {
    V,
    W,
    Y,
    Z,
    CV,
    CW,
    CY,
    CZ,
    DV,
    DW,
    DY,
    DZ,
    RV,
    RW,
    RY,
    RZ,
}

impl fmt::Display  for Origin {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        let text =  match self {
            Origin::V => "V",
            Origin::W => "W",
            Origin::Y => "Y",
            Origin::Z => "Z",
            Origin::CV => "CV",
            Origin::CW => "CW",
            Origin::CY => "CY",
            Origin::CZ => "CZ",
            Origin::DV => "DV",
            Origin::DW => "DW",
            Origin::DY => "DY",
            Origin::DZ => "DZ",
            Origin::RV => "RV",
            Origin::RW => "RW",
            Origin::RY => "RY",
            Origin::RZ => "RZ",
        };

        write!(f, "{}", text)
    }
}

/// Points of origin of conditional jumps.
/// An abbreviated form of this list is to combine V, W, Y or Z as follows:
/// /∆  -> a∆
/// c∆ -> b∆
/// d∆ -> e∆
/// r∆ -> f∆
/// ∆ = V, W, Y or Z
#[allow(non_camel_case_types)]
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum ConditionalOrigin {
    _V,
    _W,
    _Y,
    _Z,
    cV,
    cW,
    cY,
    cZ,
    dV,
    dW,
    dY,
    dZ,
    rV,
    rW,
    rY,
    rZ,
}

impl fmt::Display  for ConditionalOrigin {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        let text =  match self {
            ConditionalOrigin::_V => "/V",
            ConditionalOrigin::_W => "/W",
            ConditionalOrigin::_Y => "/Y",
            ConditionalOrigin::_Z => "/Z",
            ConditionalOrigin::cV => "cV",
            ConditionalOrigin::cW => "cW",
            ConditionalOrigin::cY => "cY",
            ConditionalOrigin::cZ => "cZ",
            ConditionalOrigin::dV => "dV",
            ConditionalOrigin::dW => "dW",
            ConditionalOrigin::dY => "dY",
            ConditionalOrigin::dZ => "dZ",
            ConditionalOrigin::rV => "rV",
            ConditionalOrigin::rW => "rW",
            ConditionalOrigin::rY => "rY",
            ConditionalOrigin::rZ => "rZ",
        };
        
        write!(f, "{}", text)
    }
}

/// Identifies the memory location as jump target 
/// for the relative jump instruction.
#[allow(non_camel_case_types)]
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum JumpDestination {
    // Destinations for jumps
    AV,
    AW,
    AY,
    AZ,
    BV,
    BW,
    BY,
    BZ,
    EV,
    EW,
    EY,
    EZ,
    FV,
    FW,
    FY,
    FZ,
    // Destinations for conditinoal jumps
    aV,
    aW,
    aY,
    aZ,
    bV,
    bW,
    bY,
    bZ,
    eV,
    eW,
    eY,
    eZ,
    fV,
    fW,
    fY,
    fZ,
}

impl fmt::Display  for JumpDestination {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let text = match self {
            JumpDestination::aV => "aV",
            JumpDestination::aW => "aW",
            JumpDestination::aY => "aY",
            JumpDestination::aZ => "aZ",
            JumpDestination::AV => "AV",
            JumpDestination::AW => "AW",
            JumpDestination::AY => "AY",
            JumpDestination::AZ => "AZ",
            JumpDestination::BV => "BV",
            JumpDestination::bV => "bV",
            JumpDestination::BW => "BW",
            JumpDestination::bW => "bW",
            JumpDestination::BY => "BY",
            JumpDestination::bY => "bY",
            JumpDestination::BZ => "BZ",
            JumpDestination::bZ => "bZ",
            JumpDestination::EV => "EV",
            JumpDestination::eV => "eV",
            JumpDestination::EW => "EW",
            JumpDestination::eW => "eW",
            JumpDestination::EY => "EY",
            JumpDestination::eY => "eY",
            JumpDestination::EZ => "EZ",
            JumpDestination::eZ => "eZ",
            JumpDestination::FV => "FV",
            JumpDestination::fV => "fV",
            JumpDestination::FW => "FW",
            JumpDestination::fW => "fW",
            JumpDestination::FY => "FY",
            JumpDestination::fY => "fY",
            JumpDestination::FZ => "FZ",
            JumpDestination::fZ => "fZ"
        };

        write!(f, "{}", text)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum CaiSign {
    Negative,
    Positive,
}

impl fmt::Display  for CaiSign {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let text = match self {
            CaiSign::Positive => "+",
            CaiSign::Negative => "-"
        };

        write!(f, "{}", text)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum CaiOrder {
    Low,
    High,
}

impl fmt::Display  for CaiOrder {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let text = match self {
            CaiOrder::High => "H",
            CaiOrder::Low => "L"
        };

        write!(f, "{}", text)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum CaiDigit {
    N1,
    N2,
    N3,
    N4,
    N5,
    N6,
    N7,
    N8,
    N9,
    N0,
}

impl From<CaiDigit> for i64 {
    fn from(d: CaiDigit) -> Self {
        match d {
            CaiDigit::N0 => 0.into(),
            CaiDigit::N1 => 1.into(),
            CaiDigit::N2 => 2.into(),
            CaiDigit::N3 => 3.into(),
            CaiDigit::N4 => 4.into(),
            CaiDigit::N5 => 5.into(),
            CaiDigit::N6 => 6.into(),
            CaiDigit::N7 => 7.into(),
            CaiDigit::N8 => 8.into(),
            CaiDigit::N9 => 9.into(),
        }
    }
}

impl fmt::Display  for CaiDigit {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let text = match self {
            CaiDigit::N0 => "0",
            CaiDigit::N1 => "1",
            CaiDigit::N2 => "2",
            CaiDigit::N3 => "3",
            CaiDigit::N4 => "4",
            CaiDigit::N5 => "5",
            CaiDigit::N6 => "6",
            CaiDigit::N7 => "7",
            CaiDigit::N8 => "8",
            CaiDigit::N9 => "9"
        };

        write!(f, "{}", text)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum CaiComma {
    No,
    Yes,
}

impl fmt::Display  for CaiComma {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let text = match self {
            CaiComma::Yes => ".",
            CaiComma::No => ""
        };

        write!(f, "{}", text)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Instruction {
    // Math
    Add(Operand),
    Sub(Operand),
    Mul(Operand),
    Div(Operand),
    Sqr(Operand),

    /// A↕ instruction: set a register to its absolute value.
    Abs,

    /// x↑ instruction: copies m register to another register.
    CopyM(Operand),

    // x↓ instruction: copies operand value in A.
    CopyToA(Operand),

    /// x↕ instruction: swaps a and operand register values.
    SwapA(Operand),

    // /↕ instruction: copies the decimal part of the accumulator to m register.
    CopyDecimal,

    /// Unconditional Jumps: jumps are executed whenever the instruction is read.
    Jump(Origin),
    
    /// Conditional jump: these jumps choose one of two alternatives by testing the
    /// contents of the A register for the following condition:
    /// GREATER THAN 0 the program jumps to the corresponding Reference Point.
    /// ZERO OR LESS - the program continues with the next instruction in sequence.
    ConditionalJump(ConditionalOrigin),

    Label(JumpDestination),

    /// x* instruction: set register value to 0
    Reset(Operand),

    /// Diamon (\u20DF) instruction: prints register
    Print(Operand),
    
    /// S instruction: waits for user input
    Stop,

    /// Vertical Spacing. The instruction "/⋄" (/\u20DF) directs the computer to advance
    /// the tape one vertical space, without printing.
    NewLine,

    /// Constant as Instructions
    CaiStart,
    Cai(CaiSign, CaiOrder, CaiDigit, CaiComma),

    /// Swap D and R registers.
    DrExchange,
}

#[cfg(test)]
mod tests {
    use crate::instruction::*;

    #[test]
    pub fn instruction_equality() {
        assert_eq!(Instruction::Add(Operand::B), Instruction::Add(Operand::B));
    }
}
