#[allow(dead_code)]
pub mod instruction;

#[allow(dead_code)]
pub mod encoding;

#[allow(dead_code)]
pub mod encoder;

#[allow(dead_code)]
pub mod decoder;

#[allow(dead_code)]
pub mod program;
