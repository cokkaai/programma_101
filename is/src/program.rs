use crate::instruction::*;

pub type Program = Vec<Instruction>;

/// Iterates over a program's instructions
pub struct ProgramIterator<'a> {
    pub instr: &'a Vec<Instruction>,
    pointer: usize
}

impl<'a> ProgramIterator<'a> {
    /// Creates a new iterator for the given program
    pub fn new(instructions: &'a Vec<Instruction>) -> Self {
        Self {
            instr: instructions,
            pointer: 0
        }
    }
}

impl<'a> Iterator for ProgramIterator<'a> {
    type Item = &'a Instruction;

    /// Returns a reference to the next instruction, if any
    fn next(&mut self) -> Option<Self::Item> {
        let instr = self.instr.get(self.pointer);
        self.pointer += 1;
        instr
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn short_program_equality() {
        let p1 = vec!(Instruction::Abs, Instruction::Add(Operand::A));
        let p2 = vec!(Instruction::Abs, Instruction::Add(Operand::A));

        assert_eq!(p1, p2);
    }
}
