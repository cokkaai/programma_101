use p101_is::instruction::*;
use p101_is::encoding::*;
use p101_is::encoder::*;
use p101_is::decoder::*;

#[test]
pub fn encoding_idempotence() {
    let p = vec!(Instruction::Label(JumpDestination::AV),
        Instruction::CopyToA(Operand::C),
        Instruction::Div(Operand::M),
        Instruction::Mul(Operand::A),
        Instruction::Div(Operand::A),
        Instruction::Div(Operand::M),
        Instruction::SwapA(Operand::c),
        Instruction::Mul(Operand::C),
        Instruction::SwapA(Operand::C),
        Instruction::Label(JumpDestination::aV),
        Instruction::NewLine,
        Instruction::Reset(Operand::A),
        Instruction::Add(Operand::d),
        Instruction::Print(Operand::R),
        Instruction::CopyToA(Operand::d),
        Instruction::CaiStart,
        Instruction::Cai(CaiSign::Positive, CaiOrder::Low, CaiDigit::N0, CaiComma::No),
        Instruction::Cai(CaiSign::Positive, CaiOrder::Low, CaiDigit::N8, CaiComma::No),
        Instruction::Cai(CaiSign::Positive, CaiOrder::High, CaiDigit::N1, CaiComma::No),
        Instruction::Div(Operand::M),
        Instruction::Mul(Operand::A),
        Instruction::SwapA(Operand::B),
        Instruction::CopyToA(Operand::B),
        Instruction::CaiStart,
        Instruction::Cai(CaiSign::Negative, CaiOrder::Low, CaiDigit::N7, CaiComma::No),
        Instruction::Cai(CaiSign::Negative, CaiOrder::Low, CaiDigit::N7, CaiComma::No),
        Instruction::Cai(CaiSign::Negative, CaiOrder::Low, CaiDigit::N5, CaiComma::No),
        Instruction::Cai(CaiSign::Negative, CaiOrder::Low, CaiDigit::N0, CaiComma::No),
        Instruction::Cai(CaiSign::Negative, CaiOrder::Low, CaiDigit::N2, CaiComma::No),
        Instruction::Cai(CaiSign::Negative, CaiOrder::Low, CaiDigit::N0, CaiComma::No),
        Instruction::Cai(CaiSign::Negative, CaiOrder::High, CaiDigit::N0, CaiComma::Yes),
        Instruction::Mul(Operand::M),
    );

    let enc = Encoder::<StandardEncoding, NullAnnotator>::new();
    let text = enc.encode(&p);

    let mut dec = Decoder::<StandardEncoding>::new();
    match dec.decode(&text) {
        Ok(p1) => {
            assert_eq!(p.len(), p1.len());

            for ip in 0..p.len() {
                assert_eq!(p.get(ip), p1.get(ip), "Equality test failed at ip {}", ip);
            }
        },
        Err(e) => {
            panic!("Decoder failed with error {}", e);
        }
    };
}
