use p101_is::instruction::Operand;
use crate::message::Key;

pub struct SelectedOperand {
    operand: Operand
}

impl SelectedOperand {
    pub fn new() -> Self {
        Self {
            operand: Operand::M
        }
    }

    pub fn update(&mut self, key: Key) {
        match key {
            Key::Split => match self.operand {
                Operand::B => self.operand = Operand::b,
                Operand::C => self.operand = Operand::c,
                Operand::D => self.operand = Operand::d,
                Operand::E => self.operand = Operand::e,
                Operand::F => self.operand = Operand::f,
                _ => ()
            },
            Key::A => self.operand = Operand::A,
            Key::B => self.operand = Operand::B,
            Key::C => self.operand = Operand::C,
            Key::D => self.operand = Operand::D,
            Key::E => self.operand = Operand::E,
            Key::F => self.operand = Operand::F,
            Key::R => self.operand = Operand::R,
            _ => ()
        }
    }

    pub fn get_operand(&mut self) -> Operand {
        let op = self.operand;
        self.operand = Operand::M;
        op
    }
}

#[cfg(test)]
mod tests {
    use crate::alu::component::selected_operand::SelectedOperand;
    use super::*;

    #[test]
    pub fn when_a_key_is_pressed_then_select_the_a_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::A);
        assert_eq!(Operand::A, selected.get_operand());
    }

    #[test]
    pub fn when_a_and_split_keys_are_pressed_then_select_the_a_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::A);
        selected.update(Key::Split);
        assert_eq!(Operand::A, selected.get_operand());
    }

    #[test]
    pub fn when_split_and_a_keys_are_pressed_then_select_the_a_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::Split);
        selected.update(Key::A);
        assert_eq!(Operand::A, selected.get_operand());
    }

    #[test]
    pub fn when_b_key_is_pressed_then_select_the_b_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::B);
        assert_eq!(Operand::B, selected.get_operand());
    }

    #[test]
    pub fn when_b_and_split_keys_are_pressed_then_select_the_split_b_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::B);
        selected.update(Key::Split);
        assert_eq!(Operand::b, selected.get_operand());
    }

    #[test]
    pub fn when_split_and_b_keys_are_pressed_then_select_the_b_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::Split);
        selected.update(Key::B);
        assert_eq!(Operand::B, selected.get_operand());
    }

    #[test]
    pub fn when_c_key_is_pressed_then_select_the_c_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::C);
        assert_eq!(Operand::C, selected.get_operand());
    }

    #[test]
    pub fn when_c_and_split_keys_are_pressed_then_select_the_split_c_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::C);
        selected.update(Key::Split);
        assert_eq!(Operand::c, selected.get_operand());
    }

    #[test]
    pub fn when_split_and_c_keys_are_pressed_then_select_the_c_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::Split);
        selected.update(Key::C);
        assert_eq!(Operand::C, selected.get_operand());
    }

    #[test]
    pub fn when_d_key_is_pressed_then_select_the_d_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::D);
        assert_eq!(Operand::D, selected.get_operand());
    }

    #[test]
    pub fn when_d_and_split_keys_are_pressed_then_select_the_split_d_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::D);
        selected.update(Key::Split);
        assert_eq!(Operand::d, selected.get_operand());
    }

    #[test]
    pub fn when_split_and_d_keys_are_pressed_then_select_the_d_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::Split);
        selected.update(Key::D);
        assert_eq!(Operand::D, selected.get_operand());
    }

    #[test]
    pub fn when_e_key_is_pressed_then_select_the_e_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::E);
        assert_eq!(Operand::E, selected.get_operand());
    }

    #[test]
    pub fn when_e_and_split_keys_are_pressed_then_select_the_split_e_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::E);
        selected.update(Key::Split);
        assert_eq!(Operand::e, selected.get_operand());
    }

    #[test]
    pub fn when_split_and_e_keys_are_pressed_then_select_the_e_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::Split);
        selected.update(Key::E);
        assert_eq!(Operand::E, selected.get_operand());
    }

    #[test]
    pub fn when_f_key_is_pressed_then_select_the_f_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::F);
        assert_eq!(Operand::F, selected.get_operand());
    }

    #[test]
    pub fn when_f_and_split_keys_are_pressed_then_select_the_split_f_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::F);
        selected.update(Key::Split);
        assert_eq!(Operand::f, selected.get_operand());
    }

    #[test]
    pub fn when_split_and_f_keys_are_pressed_then_select_the_f_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::Split);
        selected.update(Key::F);
        assert_eq!(Operand::F, selected.get_operand());
    }

    #[test]
    pub fn when_r_key_is_pressed_then_select_the_r_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::R);
        assert_eq!(Operand::R, selected.get_operand());
    }

    #[test]
    pub fn when_r_and_split_keys_are_pressed_then_select_the_split_r_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::R);
        selected.update(Key::Split);
        assert_eq!(Operand::R, selected.get_operand());
    }

    #[test]
    pub fn when_split_and_r_keys_are_pressed_then_select_the_r_register() {
        let mut selected = SelectedOperand::new();
        selected.update(Key::Split);
        selected.update(Key::R);
        assert_eq!(Operand::R, selected.get_operand());
    }

    fn selected(preset: Key, select: Key) -> SelectedOperand {
        let mut selected = SelectedOperand::new();
        selected.update(preset);
        selected.update(select);
        selected
    }

    #[test]
    pub fn ignore_unmanaged_keys() {
        assert_eq!(Operand::F, selected(Key::F, Key::One).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Two).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Three).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Four).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Five).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Six).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Seven).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Eight).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Nine).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Zero).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Comma).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Negative).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::V).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::W).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Y).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Z).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Power).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::GeneralReset).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::DecimalUp).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::DecimalDown).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::RecordProgram).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::PrintProgram).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::TapeAdvance).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::TapeRelease).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Add).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Subtract).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Multiply).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Divide).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::SquareRoot).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::CopyToA).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::CopyM).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Clear).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::Start).get_operand());
        assert_eq!(Operand::F, selected(Key::F, Key::SwapA).get_operand());
    }
}

