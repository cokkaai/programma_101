use super::*;
use crate::alu::TestAluBuilder;
use std::thread;

#[test]
pub fn when_the_clear_entry_key_is_pressed_then_invoke_alu_clear_m_method() {
    // Arrange

    let (sender, _receiver) = mpsc::channel();

    let alu = TestAluBuilder::new(sender.clone())
        .build();

    let mut component = AluComponent::new(alu, sender.clone());
    let component_sender = component.link.component_sender.clone();

    // Act

    let handle = thread::spawn(move || {
        component.run();
        return component.alu;
    });

    component_sender.send(Message::SystemStatusChanged(SystemStatus::Ready)).unwrap();
    component_sender.send(Message::KeyPressed(Key::ClearEntry)).unwrap();
    component_sender.send(Message::Halt).unwrap();

    let alu = handle.join().expect("The component should halt");

    // Assert

    assert_eq!(1, alu.log.len(), "One operation should have been logged");
    assert_eq!("clear_entry", alu.log[0], "Should record a CLEAR ENTRY operation");
}
