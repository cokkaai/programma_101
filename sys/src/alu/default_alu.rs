use crate::message::*;
use crate::alu::*;
use p101_is::instruction::*;
use std::cmp::Ordering;
use std::sync::mpsc;
use rust_decimal::{Decimal, MathematicalOps};
use rust_decimal::prelude::*;

#[derive(Debug)]
pub struct DefaultAlu {
    memory: Memory,
    system: mpsc::Sender<Message>,
    decimals: Option<u8>,
    reset_m: bool
}

impl DefaultAlu {
    const PROVIDER_NAME: &'static str = "DefaultAlu";

    pub fn new(system: mpsc::Sender<Message>) -> Self {
        Self {
            memory: Memory::new(),
            system,
            decimals: None,
            reset_m: false
        }
    }

    fn post(&self, message: Message) -> Result<(), String> {
        match self.system.send(message) {
            Ok(_) => Ok(()),
            Err(e) => Err(format!("Cannot post on system bus: {}", e))
        }
    }


    // ========= Registers management =========

    /// Returns the numeric value of a register.
    /// It returns a Decimal which is large enough
    /// to represent both registers and splits.
    fn read_register(&mut self, reg: Operand) -> Decimal {
        match reg {
            Operand::A => self.memory.a.read(),
            Operand::B => self.memory.b.read(),
            Operand::b => self.memory.b.read_left(),
            Operand::C => self.memory.c.read(),
            Operand::c => self.memory.c.read_left(),
            Operand::D => self.memory.d.read(),
            Operand::d => self.memory.d.read_left(),
            Operand::E => self.memory.e.read(),
            Operand::e => self.memory.e.read_left(),
            Operand::F => self.memory.f.read(),
            Operand::f => self.memory.f.read_left(),
            Operand::M => self.memory.m.read(),
            Operand::R => self.memory.r.read()
        }
    }

    /// Writes the value to the register.
    fn write_register(&mut self, reg: Operand, v: Decimal) -> Result<(), RegisterError> {
        match reg {
            Operand::A => self.memory.a.write(v),
            Operand::B => self.memory.b.write(v),
            Operand::b => self.memory.b.write_left(v),
            Operand::C => self.memory.c.write(v),
            Operand::c => self.memory.c.write_left(v),
            Operand::D => self.memory.d.write(v),
            Operand::d => self.memory.d.write_left(v),
            Operand::E => self.memory.e.write(v),
            Operand::e => self.memory.e.write_left(v),
            Operand::F => self.memory.f.write(v),
            Operand::f => self.memory.f.write_left(v),
            Operand::M => self.memory.m.write(v),
            Operand::R => self.memory.r.write(v)
        }
    }

    /// Returs the register's name.
    fn register_name(&self, reg: Operand) -> &str {
        match reg {
            Operand::A => "A",
            Operand::B => "B",
            Operand::b => "b",
            Operand::C => "C",
            Operand::c => "c",
            Operand::D => "D",
            Operand::d => "d",
            Operand::E => "E",
            Operand::e => "e",
            Operand::F => "F",
            Operand::f => "f",
            Operand::M => "M",
            Operand::R => "R"
        }
    }

    /// Fetch the operand into register M and returns its value.
    fn fetch_operand(&mut self, operand: Operand) -> Decimal {
        match operand {
            Operand::M => self.memory.m.read(),
            _ => {
                let v = self.read_register(operand);
                let _ =  self.write_register(Operand::M, v);
                v
            },
        }
    }

    fn set_precision(&mut self, precision: u32) {
        self.memory.precision = precision;
    }

    fn round_off(&self, value: Decimal) -> Decimal {
        match value.scale().cmp(&self.memory.precision) {
            // No rouding required if value has less decimals
            // than the required precision
            Ordering::Less => {
                let scale_factor = Decimal::new(10_i64.pow(self.memory.precision), 0);
                let v = (value * scale_factor).to_i64();
                Decimal::new(v.unwrap(), self.memory.precision)
            },
            Ordering::Equal => value,
            Ordering::Greater => {
                // Split value into integer and decimal parts
                let i = value.trunc();
                let mut f = value.fract();

                // Truncate decimals to required precision
                let scale_factor = Decimal::new(10_i64.pow(self.memory.precision), 0);
                f *= scale_factor;
                let mut f = f.trunc();
                f /= scale_factor;

                i + f
            }
        }
    }
    
    fn cai(&mut self) -> Result<(), String> {
        let mut value = 0;
        let mut scale = 0;
        let mut exp = 0;
        let mut sign_factor = 0;

        while let Some(Instruction::Cai(sign, order, digit, comma)) = self.read_instruction(self.memory.ip) {
            value += match exp {
                0 => i64::from(digit),
                exp => 10_i64.pow(exp) * i64::from(digit),
            };

            if comma == CaiComma::Yes {
                if scale == 0 {
                    scale = exp;
                } else {
                    panic!("Illegal CAI instruction at IP {}: cannot set comma twice", self.memory.ip);
                }
            }

            if sign_factor == 0 {
                sign_factor = match sign {
                    CaiSign::Positive => 1,
                    CaiSign::Negative => -1,
                };
            } else if sign_factor > 0 && sign == CaiSign::Negative {
                panic!("Illegal CAI instruction at IP {}: positive sign already set", self.memory.ip);
            } else if sign_factor < 0 && sign == CaiSign::Positive {
                panic!("Illegal CAI instruction at IP {}: negative sign already set", self.memory.ip);
            }

            exp += 1;
            self.memory.ip += 1;

            if order == CaiOrder::High {
                break;
            }
        }

        let m = Decimal::new(value * sign_factor, scale);
        let _ = self.memory.m.write(m);

        Ok(())
    }

    fn jump_to(&mut self, destination: JumpDestination) -> Result<(), String> {
        for address in 0 ..= 119 {
            if let Some(Instruction::Label(l)) = self.read_instruction(address) {
                if l == destination {
                    self.memory.ip = address;
                    return Ok(());
                }
            }
        }

        Err(format!("Jump destination {} not found", destination))
    }

    // ========= Control instructions =========

    pub fn stop(&mut self) -> Result<(), String> {
        self.post(Message::ProgramStopped)
    }
}


impl AluProvider for DefaultAlu {
    fn get_provider_name(&self) -> &'static str {
        Self::PROVIDER_NAME
    }

    // ========= Memory management =========

    fn update_memory(&mut self, program: Vec<Instruction>) -> Result<(), String> {
        let memory = Memory::from_program(&program);
        self.memory = memory;
        Ok(())
    }

    /// Reads the instruction at the specified address.
    /// The address parameter is the offset from memory location 0.
    /// On the actual machine addresses are 1-based.
    fn read_instruction(&self, address: usize) -> Option<Instruction> {
        match address {
            0 ..= 23 => self.memory.p1[address],
            24 ..= 47 => self.memory.p2[address - 24],
            48 ..= 71 => match self.memory.f.read_instruction(address - 48) {
                Ok(i) => i,
                Err(_) => None
            },
            72 ..= 95 => match self.memory.e.read_instruction(address - 72) {
                Ok(i) => i,
                Err(_) => None
            },
            96 ..= 119 => match self.memory.d.read_instruction(address - 96) {
                Ok(i) => i,
                Err(_) => None
            },
            _ => panic!("Instruction address out of bounds (0-119)"),
        }
    }

    fn read_ip(&mut self) -> usize {
        self.memory.ip
    }

    fn increment_ip(&mut self, step: usize) -> Result<(), String> {
        self.memory.ip += step;
        Ok(())
    }


    // ======== Register instructions =========

    fn copy_decimal(&mut self) -> Result<(), String> {
        let v = self.memory.a.read();
        let d = v.fract();
        match self.memory.m.write(d) {
             Ok(_) => {
                 self.memory.ip += 1;
                 Ok(())
             },
             Err(e) => Err(format!("Error copying the decimal part of A in M: {}", e))
        }
    }

    fn copy_to_a(&mut self, operand: Operand) -> Result<(), String> {
        let v = self.read_register(operand);
         match self.memory.a.write(v) {
             Ok(_) => {
                 self.memory.ip += 1;
                 Ok(())
             },
             Err(e) => Err(format!("Error copying {} in A: {}", operand, e))
         }
    }
    
    fn copy_m(&mut self, operand: Operand) -> Result<(), String> {
        match operand {
            Operand::A => {
                self.memory.ip += 1;
                self.cai()
            },
            Operand::M => {
                // Inoperative
                self.memory.ip += 1;
                Ok(())
            }, 
            Operand::R => {
                // Inoperative
                self.memory.ip += 1;
                Ok(())
            },
            _=> {
                let v = self.memory.m.read();
                match self.write_register(operand, v) {
                    Ok(_) => {
                        self.memory.ip += 1;
                        Ok(())
                    },
                    Err(e) => Err(format!("Error copying M in {}: {}", operand, e))
                }
            }
        }
    }
    
    fn print(&mut self, operand: Operand) -> Result<(), String> {
        let v = match operand {
            Operand::A => self.memory.a.read(),
            Operand::B => self.memory.b.read(),
            Operand::b => self.memory.b.read_left(),
            Operand::C => self.memory.c.read(),
            Operand::c => self.memory.c.read_left(),
            Operand::D => self.memory.d.read(),
            Operand::d => self.memory.d.read_left(),
            Operand::E => self.memory.e.read(),
            Operand::e => self.memory.e.read_left(),
            Operand::F => self.memory.f.read(),
            Operand::f => self.memory.f.read_left(),
            Operand::M => self.memory.m.read(),
            Operand::R => self.memory.r.read(),
        };

        let what = match operand {
            Operand::M => format!("{}  \u{25C6}", v),
            _ => format!("{} {}\u{25C6}", v, operand)
        };
        
        self.post(Message::PrintLine(what))
    }

    fn newline(&mut self) -> Result<(), String> {
        match self.post(Message::PrintLine("".to_string())) {
            Ok(()) => {
                self.memory.ip += 1;
                Ok(())
            },
            Err(e) => Err(e)
        }
    }

    fn reset(&mut self, operand: Operand) -> Result<(), String> {
        let value = match operand {
            Operand::M => {
                self.read_register(operand)
            },
            _ => {
                let value = self.read_register(operand);
                if let Err(e) = self.write_register(operand, Decimal::new(0, 0)) {
                    let msg = format!("Cannot write to register {}: {}", operand, e);
                    return Err(msg);
                }
                value
            },
        };

        let text = self.round_off(value).to_string()
            + " "    
            + self.register_name(operand);
        
        match self.system.send(Message::PrintLine(text)) {
            Ok(_) => Ok(()),
            Err(e) => {
                let msg = format!("Cannot post message on system bus: {}", e);
                log::error!("{}", msg);
                Err(msg)
            }
        }
    }

    fn swap_a(&mut self, operand: Operand) -> Result<(), String> {
        let a = self.memory.a.read();

        let result = match operand {
            Operand::A => Ok(()),
            Operand::B => {
                let _ = self.memory.a.write(self.memory.b.read());
                self.memory.b.write(a)
            },
            Operand::b => {
                let _ = self.memory.a.write(self.memory.b.read_left());
                self.memory.b.write_left(a)
            },
            Operand::C => {
                let _ = self.memory.a.write(self.memory.c.read());
                self.memory.c.write(a)
            },
            Operand::c => {
                let _ = self.memory.a.write(self.memory.c.read_left());
                self.memory.c.write_left(a)
            },
            Operand::D => {
                let _ = self.memory.a.write(self.memory.d.read());
                self.memory.d.write(a)
            },
            Operand::d => {
                let _ = self.memory.a.write(self.memory.d.read_left());
                self.memory.d.write_left(a)
            },
            Operand::E => {
                let _ = self.memory.a.write(self.memory.e.read());
                self.memory.e.write(a)
            },
            Operand::e => {
                let _ = self.memory.a.write(self.memory.e.read_left());
                self.memory.e.write_left(a)
            },
            Operand::F => {
                let _ = self.memory.a.write(self.memory.f.read());
                self.memory.f.write(a)
            },
            Operand::f => {
                let _ = self.memory.a.write(self.memory.f.read_left());
                self.memory.f.write_left(a)
            },
            Operand::M => {
                let _ = self.memory.a.write(self.memory.m.read());
                self.memory.m.write(a)
            },
            Operand::R => panic!("Illegal instruction: swap a and r")
        };

        match result {
            Ok(_) => Ok(()),
            Err(e) => Err(e.to_string())
        }
    }
    
    /// Exchange value of D and R registers.
    /// As D is a memory register (it cancontain a long data, be split or holding instructions),
    /// R must conform too. Instructions are not copied from D to R
    fn dr_exchange(&mut self) -> Result<(), String> {
        self.memory.r = self.memory.d.exchange(self.memory.r);
        Ok(())
    }

    fn clear_entry(&mut self) -> Result<(), String> {
        let m = Decimal::new(0, 0);
        self.memory.m.write(m)?;
        self.post(Message::Print("".to_string()))
    }

    fn write_to_m(&mut self, value: Digit) -> Result<(), String> {
        let mut m = if self.reset_m {
            Decimal::new(0, 0)
        } else {
            self.memory.m.read()
        };
        let msg = value.to_string();

        if let Some(mut decimals) = self.decimals {
            decimals += 1;
            self.decimals = Some(decimals);
            m += Decimal::new(value.into(), decimals.into());
        } else {
            m *= Decimal::new(10, 0);
            m += Decimal::new(value.into(), 0);
        }

        self.memory.m.write(m)?;
        self.post(Message::Print(msg))
    }

    fn entering_decimals(&mut self, value: bool) {
        self.decimals = if value {
            Some(0)
        } else {
            None
        };

        self.post(Message::Print(".".to_string())).unwrap();
    }

    fn invert_sign(&mut self) {
        let m = self.memory.m.read() * Decimal::new(-1, 0);
        self.memory.m.write(m).unwrap();

        self.post(Message::PrintLine("".to_string())).unwrap();
        self.post(Message::Print(m.to_string())).unwrap();
    }

    // ======== Math instructions =========

    fn add(&mut self, operand: Operand) -> Result<(), String> {
        let v = self.fetch_operand(operand);

        match self.memory.a.read().checked_add(v) {
            Some(r) => match self.memory.r.write(r)
                .and_then(|_| {
                    self.reset_m = true;
                    self.memory.a.write(self.round_off(r))
                 }) {
                    Ok(_) => Ok(()),
                    Err(e) => Err(format!("Error adding {} to A: {}", operand, e))
            },
            None => {
                Err(format!("{} + {} overflowed", self.memory.a.read(), v))
            }
        }
    }

    fn subtract(&mut self, operand: Operand) -> Result<(), String> {
        let v = self.fetch_operand(operand);
        
        match self.memory.a.read().checked_sub(v) {
            Some(r) => match self.memory.r.write(r)
                .and_then(|_| {
                    self.reset_m = true;
                    self.memory.a.write(self.round_off(r))
                 }) {
                    Ok(_) => Ok(()),
                    Err(e) => Err(format!("Error subtracting {} from A: {}", operand, e))
            },
            None => {
                Err(format!("{} - {} overflowed", self.memory.a.read(), v))
            }
        }
    }

    fn multiply(&mut self, operand: Operand) -> Result<(), String> {
        let v = self.fetch_operand(operand);

        match self.memory.a.read().checked_mul(v) {
            Some(r) => match self.memory.r.write(r)
                .and_then(|_| {
                    self.reset_m = true;
                    self.memory.a.write(self.round_off(r))
                 }) {
                    Ok(_) => Ok(()),
                    Err(e) => Err(format!("Error multiplying {} by A: {}", operand, e))
            },
            None => {
                Err(format!("{} * {} overflowed", self.memory.a.read(), v))
            }
        }

    }

    fn divide(&mut self, operand: Operand) -> Result<(), String> {
        let v = self.fetch_operand(operand);

        if v == Decimal::new(0, 0) {
            let _ = self.system.send(Message::Error);
        }
        
        let q = self.memory.a.read().checked_div(v);

        if q.is_none() {
            return Err(format!("{} / {} overflowed", self.memory.a.read(), v));
        }

        let q = self.round_off(q.unwrap());

        let r = self.memory.a.read() - q * v;

        match self.memory.r.write(r)
            .and_then(|_| {
                self.reset_m = true;
                self.memory.a.write(q)
            }) {
                Ok(_) => Ok(()),
                Err(e) => Err(format!("Error dividing A by {}: {}", operand, e))
            }
    }

    fn sqr(&mut self, operand: Operand) -> Result<(), String> {
        let v = self.fetch_operand(operand);

        if operand != Operand::M {
            self.write_register(Operand::M, self.round_off(v))?;
        }

        if let Some(res) = v.sqrt() {
            self.write_register(Operand::A, self.round_off(res))?;
            self.reset_m = true;
            Ok(())
        } else {
            Err(format!("Cannot calculate sqr({})", v))
        }
    }

    fn abs(&mut self) -> Result<(), String> {
        let v = self.memory.a.read();

        if v < Decimal::new(0, 0) {
            // TODO: can abs produce an overflow or is it safe to ignore the error?
            if let Err(e) = self.memory.a.write(-v) {
                return Err(format!("Error calculating absolute of {}: {}", v, e));
            }
        }

        Ok(())
    }

    // ========= Jump instructions =========

    fn label(&mut self) -> Result<(), String> {
        // NOTE: Not sure if running a label is an error 
        // or the instruction is simply skipped.
        Ok(())
    }

    fn jump(&mut self, origin: Origin) -> Result<(), String> {
        let destination = match origin {
            Origin::V => JumpDestination::AV,
            Origin::W => JumpDestination::AW,
            Origin::Y => JumpDestination::AY,
            Origin::Z => JumpDestination::AZ,
            Origin::CV => JumpDestination::BV,
            Origin::CW => JumpDestination::BW,
            Origin::CY => JumpDestination::BY,
            Origin::CZ => JumpDestination::BZ,
            Origin::DV => JumpDestination::EV,
            Origin::DW => JumpDestination::EW,
            Origin::DY => JumpDestination::EY,
            Origin::DZ => JumpDestination::EZ,
            Origin::RV => JumpDestination::FV,
            Origin::RW => JumpDestination::FW,
            Origin::RY => JumpDestination::FY,
            Origin::RZ => JumpDestination::FZ,
        };

        self.jump_to(destination)
    }

    fn conditional_jump(&mut self, origin: ConditionalOrigin) -> Result<(), String> {
        if self.read_register(Operand::A) > 0.into() {
            let destination = match origin {
                ConditionalOrigin::_V => JumpDestination::aV,
                ConditionalOrigin::_W => JumpDestination::aW,
                ConditionalOrigin::_Y => JumpDestination::aY,
                ConditionalOrigin::_Z => JumpDestination::aZ,
                ConditionalOrigin::cV => JumpDestination::bV,
                ConditionalOrigin::cW => JumpDestination::bW,
                ConditionalOrigin::cY => JumpDestination::bY,
                ConditionalOrigin::cZ => JumpDestination::bZ,
                ConditionalOrigin::dV => JumpDestination::eV,
                ConditionalOrigin::dW => JumpDestination::eW,
                ConditionalOrigin::dY => JumpDestination::eY,
                ConditionalOrigin::dZ => JumpDestination::eZ,
                ConditionalOrigin::rV => JumpDestination::fV,
                ConditionalOrigin::rW => JumpDestination::fW,
                ConditionalOrigin::rY => JumpDestination::fY,
                ConditionalOrigin::rZ => JumpDestination::fZ,
            };
    
            self.jump_to(destination)
        } else {
            self.memory.ip += 1;
            Ok(())
        }
    }

    fn cai_start(&mut self) -> Result<(), String> {
        self.memory.ip += 1;
        self.cai()
    }

    fn clear_memory(&mut self) {
        self.memory = Memory::new();
    }

    fn start_stop(&mut self) -> Result<(), String> {
        self.memory.ip += 1;
        Ok(())
    }
}

#[cfg(test)]
impl DefaultAlu {
    pub fn new_with_memory(system: mpsc::Sender<Message>, memory: Memory) -> Self {
        Self {
            memory: memory,
            system,
            decimals: None,
            reset_m: false
        }
    }
}

#[cfg(test)]
mod abs;

#[cfg(test)]
mod cai;

#[cfg(test)]
mod conditional_jump;

#[cfg(test)]
mod copy_decimal;

#[cfg(test)]
mod copy_m;

#[cfg(test)]
mod copy_to_a;

#[cfg(test)]
mod dr_exchange;

#[cfg(test)]
mod jump;

#[cfg(test)]
mod math;

#[cfg(test)]
mod newline;

#[cfg(test)]
mod print;

#[cfg(test)]
mod reset;

#[cfg(test)]
mod rounding;

#[cfg(test)]
mod swap_a;

#[cfg(test)]
mod write_to_m;

// #[cfg(test)]
// mod running;

