use crate::alu::*;
use std::sync::mpsc;
use rust_decimal::Decimal;

#[test]
fn abs_of_zero_value_does_not_alter_the_register() {

    //  Arrange

    let (sender, _system) = mpsc::channel();
    let mut alu = DefaultAlu::new(sender);

    // Act

    let result = alu.abs();
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.a.read());
}

#[test]
fn abs_of_a_positive_value_does_not_alter_the_register() {

    //  Arrange

    let memory = MemoryBuilder::new()
        .with_a(Decimal::new(1, 0))
        .with_precision(2)
        .build();

    let (sender, _system) = mpsc::channel();
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.abs();
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(1, 0), alu.memory.a.read());
}

#[test]
fn abs_on_a_negative_value_negates_register_value() {

    //  Arrange

    let memory = MemoryBuilder::new()
        .with_a(Decimal::new(-1, 0))
        .with_precision(2)
        .build();

    let (sender, _system) = mpsc::channel();
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.abs();
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(1, 0), alu.memory.a.read());
}
