use crate::message::*;
use crate::alu::*;
use p101_is::instruction::*;
use std::sync::mpsc;
use rust_decimal::Decimal;

#[test]
pub fn seven_plus_three_equals_ten() {

    //Arrange

    let (sender, _) = mpsc::channel();

    let memory = MemoryBuilder::new()
        .with_m(5u8.into())
        .with_a(7u8.into())
        .with_r(9u8.into())
        .with_b(Decimal::new(3, 0).into())
        .with_precision(0)
        .build();
        
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.add(Operand::B);

    // Assert
    
    assert!(result.is_ok());

    assert_eq!(Decimal::new(3, 0), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(10, 0), alu.read_register(Operand::A));
    assert_eq!(Decimal::new(10, 0), alu.read_register(Operand::R));
    assert_eq!(Decimal::new(3, 0), alu.read_register(Operand::B));
}

#[test]
pub fn seven_dot_five_minus_three_dot_six_equals_three_dot_nine() {

    //Arrange
    
    let (sender, _) = mpsc::channel();

    let memory = MemoryBuilder::new()
        .with_m(5u8.into())
        .with_a(Decimal::new(75, 1))
        .with_r(9u8.into())
        .with_b(Decimal::new(36, 1).into())
        .with_precision(0)
        .build();
        
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.subtract(Operand::B);

    // Assert
    
    assert!(result.is_ok());

    assert_eq!(Decimal::new(36, 1), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(3, 0), alu.read_register(Operand::A));
    assert_eq!(Decimal::new(39, 1), alu.read_register(Operand::R));
    assert_eq!(Decimal::new(36, 1), alu.read_register(Operand::B));
}

#[test]
pub fn seven_by_six_dot_four_equals_33_dot_28() {

    //Arrange

    let (sender, _) = mpsc::channel();

    let memory = MemoryBuilder::new()
        .with_m(5u8.into())
        .with_a(Decimal::new(52, 1))
        .with_r(9u8.into())
        .with_b(Decimal::new(64, 1).into())
        .with_precision(0)
        .build();
        
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.multiply(Operand::B);

    // Assert
    
    assert!(result.is_ok());

    assert_eq!(Decimal::new(64, 1), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(33, 0), alu.read_register(Operand::A));
    assert_eq!(Decimal::new(3328, 2), alu.read_register(Operand::R));
    assert_eq!(Decimal::new(64, 1), alu.read_register(Operand::B));
}

#[test]
pub fn seven_divided_three_equals_two_remainder_one() {

    //Arrange

    let (sender, _) = mpsc::channel();

    let memory = MemoryBuilder::new()
        .with_m(5u8.into())
        .with_a(7u8.into())
        .with_r(9u8.into())
        .with_b(Decimal::new(3, 0).into())
        .with_precision(0)
        .build();
        
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.divide(Operand::B);

    // Assert
    
    assert!(result.is_ok());

    assert_eq!(Decimal::new(3, 0), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(2, 0), alu.read_register(Operand::A));
    assert_eq!(Decimal::new(1, 0), alu.read_register(Operand::R));
    assert_eq!(Decimal::new(3, 0), alu.read_register(Operand::B));
}

#[test]
pub fn dividing_by_zero_turns_on_the_error_light() {

    //Arrange

    let (sender, system_receiver) = mpsc::channel();

    let memory = MemoryBuilder::new()
        .with_m(5u8.into())
        .with_a(7u8.into())
        .with_r(9u8.into())
        .with_b(Decimal::new(0, 0).into())
        .with_precision(0)
        .build();
        
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let _ = alu.divide(Operand::B);
    let error = system_receiver.try_recv();

    // Assert
    
    assert_eq!(Ok(Message::Error), error);
}

#[test]
pub fn sqr_of_four_is_two() {

    //Arrange

    let (sender, _) = mpsc::channel();

    let memory = MemoryBuilder::new()
        .with_m(4u8.into())
        .with_a(98u8.into())
        .with_r(97u8.into())
        .with_precision(1)
        .build();
        
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.sqr(Operand::M);

    // Assert
    
    assert!(result.is_ok());

    assert_eq!(Decimal::new(4, 0), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(2, 0), alu.read_register(Operand::A));

    alu.set_precision(2);
    assert_eq!(Decimal::new(4, 0), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(2, 0), alu.read_register(Operand::A));

    alu.set_precision(3);
    assert_eq!(Decimal::new(4, 0), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(2, 0), alu.read_register(Operand::A));

    alu.set_precision(4);
    assert_eq!(Decimal::new(4, 0), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(2, 0), alu.read_register(Operand::A));

    alu.set_precision(5);
    assert_eq!(Decimal::new(4, 0), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(2, 0), alu.read_register(Operand::A));

    alu.set_precision(15);
    assert_eq!(Decimal::new(4, 0), alu.read_register(Operand::M));
    assert_eq!(Decimal::new(2, 0), alu.read_register(Operand::A));
}

#[test]
pub fn sqr_of_five_is_2_236067977499789() {

    //Arrange

    let (sender, _) = mpsc::channel();

    let memory = MemoryBuilder::new()
        .with_m(5u8.into())
        .with_precision(15)
        .build();
        
    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.sqr(Operand::M);

    // Assert
    
    assert!(result.is_ok());

    assert_eq!(Decimal::new(2236067977499789, 15), alu.read_register(Operand::A));
}
