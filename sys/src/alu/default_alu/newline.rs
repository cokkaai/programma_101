use crate::alu::*;
use crate::message::*;
use std::sync::mpsc;
use std::time::Duration;

#[test]
fn printing_a_newline_increments_ip() {

    // Arrange
    
    let (sender, system) = mpsc::channel();

    let memory = MemoryBuilder::new()
        .with_ip(1)
        .build();

    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let result = alu.newline();

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(2, alu.memory.ip);

    let msg = system.recv_timeout(Duration::from_secs(1));
    assert_eq!(Ok(Message::PrintLine("".to_string())), msg);
}
