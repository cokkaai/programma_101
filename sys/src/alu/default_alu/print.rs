use crate::alu::*;
use crate::message::*;
use p101_is::instruction::*;
use std::sync::mpsc;
use std::time::*;
use rust_decimal::Decimal;

fn build_alu(memory: Memory) -> (DefaultAlu, mpsc::Receiver<Message>) {
    let (sender, receiver) = mpsc::channel();

    let alu = DefaultAlu::new_with_memory(sender, memory);

    (alu, receiver)
}

#[test]
pub fn printing_register_a_posts_its_value_on_the_system_bus() {

    // Arrange

    let memory = MemoryBuilder::new()
            .with_a(Decimal::new(12345, 2))
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::A);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 A\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_register_m_posts_its_value_on_the_system_bus() {

    // Arrange

    let memory = MemoryBuilder::new()
            .with_m(Decimal::new(12345, 2))
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::M);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45  \u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_register_r_posts_its_value_on_the_system_bus() {

    // Arrange

    let memory = MemoryBuilder::new()
            .with_r(Decimal::new(12345, 2))
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::R);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 R\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_register_b_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = SplitRegister::new();
    reg.write(Decimal::new(12345, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_b(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::B);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 B\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_split_register_a_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = SplitRegister::new();
    reg.write_left(Decimal::new(123, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_b(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::b);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("1.23 B/\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_register_c_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = SplitRegister::new();
    reg.write(Decimal::new(12345, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_c(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::C);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 C\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_split_register_c_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = SplitRegister::new();
    reg.write_left(Decimal::new(123, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_c(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::c);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("1.23 C/\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_register_d_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = MemoryRegister::new();
    reg.write(Decimal::new(12345, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_d(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::D);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 D\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_split_register_d_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = MemoryRegister::new();
    reg.write_left(Decimal::new(12345, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_d(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::d);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 D/\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_register_e_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = MemoryRegister::new();
    reg.write(Decimal::new(12345, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_e(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::E);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 E\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_split_register_e_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = MemoryRegister::new();
    reg.write_left(Decimal::new(12345, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_e(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::e);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 E/\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_register_f_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = MemoryRegister::new();
    reg.write(Decimal::new(12345, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_f(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::F);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 F\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}

#[test]
pub fn printing_split_register_f_posts_its_value_on_the_system_bus() {

    // Arrange

    let mut reg = MemoryRegister::new();
    reg.write_left(Decimal::new(12345, 2)).unwrap();

    let memory = MemoryBuilder::new()
            .with_f(reg)
            .with_precision(2)
            .with_ip(1)
            .build();

    let (mut alu, receiver) = build_alu(memory);

    // Act

    let result = alu.print(Operand::f);
    let message = receiver.recv_timeout(Duration::from_secs(1));

    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Ok(Message::PrintLine("123.45 F/\u{25C6}".to_string())), message);
    assert_eq!(1, alu.memory.ip);
}
