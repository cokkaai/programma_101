use crate::alu::*;
use crate::message::*;
use p101_is::instruction::*;
use rust_decimal::Decimal;
use std::sync::mpsc;
use std::time;

fn alu_with_memory(memory: Memory) -> (DefaultAlu, mpsc::Receiver<Message>) {
    let (sender, receiver) = mpsc::channel();

    let alu = DefaultAlu::new_with_memory(sender, memory);

    (alu, receiver)
}

#[test]
fn resetting_the_accumulator_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_a(Decimal::new(1123, 3))
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::A);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.a.read());
    assert_eq!(Ok(Message::PrintLine("1.12 A".to_string())), msg);
}

#[test]
fn resetting_the_b_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_b(Decimal::new(1123, 3).into())
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::B);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.b.read());
    assert_eq!(Ok(Message::PrintLine("1.12 B".to_string())), msg);
}

#[test]
fn resetting_the_b_split_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_b((Decimal::new(1123, 3), Decimal::new(2, 0)).into())
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::b);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.b.read_left());
    assert_eq!(Decimal::new(2, 0), alu.memory.b.read());
    assert_eq!(Ok(Message::PrintLine("1.12 b".to_string())), msg);

    let result = alu.reset(Operand::B);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.b.read_left());
    assert_eq!(Decimal::new(0, 0), alu.memory.b.read());
    assert_eq!(Ok(Message::PrintLine("2.00 B".to_string())), msg);
}

#[test]
fn resetting_the_c_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_c(Decimal::new(1123, 3).into())
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::C);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.c.read());
    assert_eq!(Ok(Message::PrintLine("1.12 C".to_string())), msg);
}

#[test]
fn resetting_the_c_split_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_c((Decimal::new(1123, 3), Decimal::new(2, 0)).into())
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::c);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.c.read_left());
    assert_eq!(Decimal::new(2, 0), alu.memory.c.read());
    assert_eq!(Ok(Message::PrintLine("1.12 c".to_string())), msg);

    let result = alu.reset(Operand::C);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.c.read_left());
    assert_eq!(Decimal::new(0, 0), alu.memory.c.read());
    assert_eq!(Ok(Message::PrintLine("2.00 C".to_string())), msg);
}

#[test]
fn resetting_the_d_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_d(Decimal::new(1123, 3).into())
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::D);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.d.read());
    assert_eq!(Ok(Message::PrintLine("1.12 D".to_string())), msg);
}

#[test]
fn resetting_the_d_split_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_d((Decimal::new(1123, 3), Decimal::new(2, 0)).into())
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::d);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.d.read_left());
    assert_eq!(Decimal::new(2, 0), alu.memory.d.read());
    assert_eq!(Ok(Message::PrintLine("1.12 d".to_string())), msg);

    let result = alu.reset(Operand::D);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.d.read_left());
    assert_eq!(Decimal::new(0, 0), alu.memory.d.read());
    assert_eq!(Ok(Message::PrintLine("2.00 D".to_string())), msg);
}

#[test]
fn resetting_the_e_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_e(Decimal::new(1123, 3).into())
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::E);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.e.read());
    assert_eq!(Ok(Message::PrintLine("1.12 E".to_string())), msg);
}

#[test]
fn resetting_the_e_split_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_e((Decimal::new(1123, 3), Decimal::new(2, 0)).into())
        .with_precision(2)
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::e);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.e.read_left());
    assert_eq!(Decimal::new(2, 0), alu.memory.e.read());
    assert_eq!(Ok(Message::PrintLine("1.12 e".to_string())), msg);

    let result = alu.reset(Operand::E);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.e.read_left());
    assert_eq!(Decimal::new(0, 0), alu.memory.e.read());
    assert_eq!(Ok(Message::PrintLine("2.00 E".to_string())), msg);
}

#[test]
fn resetting_the_f_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_precision(2)
        .with_f(Decimal::new(1123, 3).into())
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::F);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.f.read());
    assert_eq!(Ok(Message::PrintLine("1.12 F".to_string())), msg);
}

#[test]
fn resetting_the_f_split_register_writes_a_zero_in_it_and_prints_its_previous_value() {
    let memory = MemoryBuilder::new()
        .with_precision(2)
        .with_f((Decimal::new(1123, 3), Decimal::new(2, 0)).into())
        .build();

    let (mut alu, system) = alu_with_memory(memory);

    let result = alu.reset(Operand::f);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.f.read_left());
    assert_eq!(Decimal::new(2, 0), alu.memory.f.read());
    assert_eq!(Ok(Message::PrintLine("1.12 f".to_string())), msg);

    let result = alu.reset(Operand::F);
    let msg = system.recv_timeout(time::Duration::from_secs(3));

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(0, 0), alu.memory.f.read_left());
    assert_eq!(Decimal::new(0, 0), alu.memory.f.read());
    assert_eq!(Ok(Message::PrintLine("2.00 F".to_string())), msg);
}
