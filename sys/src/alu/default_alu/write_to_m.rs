use crate::alu::*;
use crate::message::*;
use p101_is::instruction::*;
use std::sync::mpsc;
use std::time::Duration;
use rust_decimal::Decimal;

#[test]
fn when_a_numeric_key_is_pressed_then_the_m_register_should_be_updated() {

    // Arrange
    
    let (sender, _system) = mpsc::channel();
    let mut alu = DefaultAlu::new(sender);

    // Act

    let result = alu.write_to_m(2.into());
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(2, 0), alu.memory.m.read());
    assert_eq!(None, alu.decimals);
}

#[test]
fn when_several_numeric_keys_are_pressed_then_the_m_register_should_be_updated_accordingly() {

    // Arrange
    
    let (sender, _system) = mpsc::channel();
    let mut alu = DefaultAlu::new(sender);

    // Act

    let _ = alu.write_to_m(1.into());
    let _ = alu.write_to_m(2.into());
    let _ = alu.write_to_m(3.into());
    
    // Assert

    assert_eq!(Decimal::new(123, 0), alu.memory.m.read());
    assert_eq!(None, alu.decimals);
}

#[test]
fn when_decimals_are_entered_then_the_m_register_should_be_updated_accordingly() {

    // Arrange
    
    let (sender, _system) = mpsc::channel();
    let mut alu = DefaultAlu::new(sender);

    // Act

    let _ = alu.write_to_m(1.into());
    let _ = alu.write_to_m(2.into());
    alu.entering_decimals(true);
    let _ = alu.write_to_m(3.into());
    let _ = alu.write_to_m(4.into());
    let _ = alu.write_to_m(5.into());
    
    // Assert

    assert_eq!(Decimal::new(12345, 3), alu.memory.m.read());
}

#[test]
fn for_any_pressed_numeric_key_a_print_message_should_be_emitted() {

    // Arrange
    
    let (sender, system) = mpsc::channel();
    let mut alu = DefaultAlu::new(sender);

    // Act

    let _ = alu.write_to_m(1.into());
    let _ = alu.write_to_m(2.into());
    alu.entering_decimals(true);
    let _ = alu.write_to_m(3.into());
    
    // Assert

    let mut msg = system.recv_timeout(Duration::from_millis(1));
    assert_eq!(Ok(Message::Print("1".to_string())), msg);

    msg = system.recv_timeout(Duration::from_millis(1));
    assert_eq!(Ok(Message::Print("2".to_string())), msg);

    msg = system.recv_timeout(Duration::from_millis(1));
    assert_eq!(Ok(Message::Print(".".to_string())), msg);

    msg = system.recv_timeout(Duration::from_millis(1));
    assert_eq!(Ok(Message::Print("3".to_string())), msg);
}

#[test]
fn when_an_addition_is_executed_then_the_m_register_should_be_overwritten() {

    // Arrange
    
    let (sender, _system) = mpsc::channel();

    let memory = MemoryBuilder::new()
            .with_a(12.into())
            .with_m(3.into())
            .build();

    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let _ = alu.add(Operand::M);
    let result = alu.write_to_m(2.into());
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(2, 0), alu.memory.m.read());
    assert_eq!(None, alu.decimals);
}

#[test]
fn when_a_subtraction_is_executed_then_the_m_register_should_be_overwritten() {

    // Arrange
    
    let (sender, _system) = mpsc::channel();

    let memory = MemoryBuilder::new()
            .with_a(12.into())
            .with_m(3.into())
            .build();

    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let _ = alu.subtract(Operand::M);
    let result = alu.write_to_m(2.into());
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(2, 0), alu.memory.m.read());
    assert_eq!(None, alu.decimals);
}

#[test]
fn when_a_multiplication_is_executed_then_the_m_register_should_be_overwritten() {

    // Arrange
    
    let (sender, _system) = mpsc::channel();

    let memory = MemoryBuilder::new()
            .with_a(12.into())
            .with_m(3.into())
            .build();

    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let _ = alu.multiply(Operand::M);
    let result = alu.write_to_m(2.into());
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(2, 0), alu.memory.m.read());
    assert_eq!(None, alu.decimals);
}

#[test]
fn when_a_division_is_executed_then_the_m_register_should_be_overwritten() {

    // Arrange
    
    let (sender, _system) = mpsc::channel();

    let memory = MemoryBuilder::new()
            .with_a(12.into())
            .with_m(3.into())
            .build();

    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let _ = alu.divide(Operand::M);
    let result = alu.write_to_m(2.into());
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(2, 0), alu.memory.m.read());
    assert_eq!(None, alu.decimals);
}


#[test]
fn when_a_square_root_is_executed_then_the_m_register_should_be_overwritten() {

    // Arrange
    
    let (sender, _system) = mpsc::channel();

    let memory = MemoryBuilder::new()
            .with_m(9.into())
            .build();

    let mut alu = DefaultAlu::new_with_memory(sender, memory);

    // Act

    let _ = alu.sqr(Operand::M);
    let result = alu.write_to_m(2.into());
    
    // Assert

    assert_eq!(Ok(()), result);
    assert_eq!(Decimal::new(2, 0), alu.memory.m.read());
    assert_eq!(None, alu.decimals);
}

