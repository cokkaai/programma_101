use crate::message::Key;
use rust_decimal::Decimal;
use std::fmt;

#[derive(Debug)]
pub enum Digit {
    Zero,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine
}

impl fmt::Display for Digit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Digit::Zero => 0,
            Digit::One => 1,
            Digit::Two => 2,
            Digit::Three => 3,
            Digit::Four => 4,
            Digit::Five => 5,
            Digit::Six => 6,
            Digit::Seven => 7,
            Digit::Eight => 8,
            Digit::Nine => 9
        };

        write!(f, "{}", s)
    }
}

impl From<i32> for Digit {
    fn from(digit: i32) -> Self {
        match digit {
            0i32 => Digit::Zero,
            1i32 => Digit::One,
            2i32 => Digit::Two,
            3i32 => Digit::Three,
            4i32 => Digit::Four,
            5i32 => Digit::Five,
            6i32 => Digit::Six,
            7i32 => Digit::Seven,
            8i32 => Digit::Eight,
            9i32 => Digit::Nine,
            _ => panic!("Cannot convert {:?} to a Digit because it is out of range", digit)
        }
    }
}

impl From<Key> for Digit {
    fn from(digit: Key) -> Self {
        match digit {
            Key::Zero => Digit::Zero,
            Key::One => Digit::One,
            Key::Two => Digit::Two,
            Key::Three => Digit::Three,
            Key::Four => Digit::Four,
            Key::Five => Digit::Five,
            Key::Six => Digit::Six,
            Key::Seven => Digit::Seven,
            Key::Eight => Digit::Eight,
            Key::Nine => Digit::Nine,
            _ => panic!("Cannot convert {:?} to a Digit because it is out of range", digit)
        }
    }
}

impl From<Digit> for Decimal {
    fn from(digit: Digit) -> Self {
        match digit {
            Digit::Zero => Decimal::new(0, 0),
            Digit::One => Decimal::new(1, 0),
            Digit::Two => Decimal::new(2, 0),
            Digit::Three => Decimal::new(3, 0),
            Digit::Four => Decimal::new(4, 0),
            Digit::Five => Decimal::new(5, 0),
            Digit::Six => Decimal::new(6, 0),
            Digit::Seven => Decimal::new(7, 0),
            Digit::Eight => Decimal::new(8, 0),
            Digit::Nine => Decimal::new(9, 0)
        }
    }
}

impl From<Digit> for i64 {
    fn from(digit: Digit) -> Self {
        match digit {
            Digit::Zero => 0i64,
            Digit::One => 1i64,
            Digit::Two => 2i64,
            Digit::Three => 3i64,
            Digit::Four => 4i64,
            Digit::Five => 5i64,
            Digit::Six => 6i64,
            Digit::Seven => 7i64,
            Digit::Eight => 8i64,
            Digit::Nine => 9i64,
        }
    }
}
