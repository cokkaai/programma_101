use std::fmt::{Display, Formatter};

/// Errors that may occur assigning a value to a register.
#[derive(Debug, PartialEq, Eq)]
pub enum RegisterError {
    /// The value overflows the capacity of a split register
    Overflow,

    /// The register cannot be split because it already contains data
    CannotSplit,

    // The register does not contain such a memory location
    NoMemory,
}

impl Display for RegisterError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            RegisterError::Overflow => write!(f, "Register overflow"),
            RegisterError::CannotSplit => write!(f, "Cannot split register"),
            RegisterError::NoMemory => write!(f, "No memory location")
        }
    }
}

impl From<RegisterError> for String {
    fn from(e: RegisterError) -> String {
        match e {
            RegisterError::Overflow => "Register overflow".to_string(),
            RegisterError::CannotSplit => "Cannot split register".to_string(),
            RegisterError::NoMemory => "No memory location".to_string()
        }
    }
}
