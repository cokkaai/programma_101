use crate::message::*;

mod cli_gui;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum PerformanceLightStatus {
    /// Machine ready for operator command
    Steady,

    /// Running program
    Flickering,

    Off
}

/// The Gui trait defines methods to update the GUI
/// according to events dispatched by the system.
/// Any UI have to implements update methods to property
/// render the status of the system.
pub trait Gui {
    /// Updates the UI with the new precision setting
    fn update_precision(&mut self, precision: u8);

    /// Updates the UI representation of the record program switch
    fn update_record_program_switch(&mut self, status: bool);

    /// Updates the UI representation of the print program switch
    fn update_print_program_switch(&mut self, status: bool);

    /// Updates the UI representation of the power switch
    fn update_power_switch(&mut self, status: bool);

    /// Updates the UI representation of the error light
    fn update_error_light(&mut self, status: bool);

    /// Updates the UI representation of the performance light
    fn update_performance_light(&mut self, status: PerformanceLightStatus);

    /// Process events dispatched by the system
    fn on_message(&mut self, message: &Message) -> Vec<Message> {
        match message {
            Message::SetPrecision(precision) => self.update_precision(*precision),
            Message::RecordProgramSwitch(status) => self.update_record_program_switch(*status),
            Message::PrintProgramSwitch(status) => self.update_print_program_switch(*status),
            Message::Error => self.update_error_light(true),

            Message::SystemStatusChanged(SystemStatus::Off) => {
                self.update_power_switch(false);
                self.update_error_light(false);
                self.update_performance_light(PerformanceLightStatus::Off);
            },

            Message::SystemStatusChanged(SystemStatus::On) => {
                self.update_power_switch(true);
                self.update_error_light(true);
                self.update_performance_light(PerformanceLightStatus::Off);
            },

            Message::SystemStatusChanged(SystemStatus::Ready) => {
                self.update_error_light(false);
                self.update_performance_light(PerformanceLightStatus::Steady);
            },

            Message::KeyPressed(Key::GeneralReset) => {
                self.update_error_light(false);
                self.update_performance_light(PerformanceLightStatus::Steady);
            },

            Message::ProgramStarted => self.update_performance_light(PerformanceLightStatus::Flickering),

            Message::ProgramStopped => self.update_performance_light(PerformanceLightStatus::Steady),

            Message::ProgramEnded => self.update_performance_light(PerformanceLightStatus::Steady),

            _ => ()
        }

        Vec::new()
    }
}
