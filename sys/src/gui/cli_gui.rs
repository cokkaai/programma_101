use std::sync::mpsc;
use crate::gui::{Gui, PerformanceLightStatus};
use crate::message::*;
use crate::system::Component;

#[derive(Debug)]
pub struct CliGui {
    // precision: u8,
    // record_program_switch: bool,
    // print_program_switch: bool,
    // power_switch: bool,
    // error_light: bool,
    // performance_light: PerformanceLightStatus   
}

impl Gui for CliGui {
    fn update_precision(&mut self, _precision: u8) {
        todo!()
    }
    
    fn update_record_program_switch(&mut self, _status: bool) {
        todo!()
    }
    
    fn update_print_program_switch(&mut self, _status: bool) {
        todo!()
    }
    
    fn update_power_switch(&mut self, _status: bool) {
        todo!()
    }
    
    fn update_error_light(&mut self, _status: bool) {
        todo!()
    }

    fn update_performance_light(&mut self, _status: PerformanceLightStatus) {
        todo!()
    }
}

impl Component for CliGui {
    fn get_name(&self) -> &str {
        "CliGui"
    }

    fn get_sender(&self) -> mpsc::Sender<Message> {
        unimplemented!();
    }

    fn listen_to(&self) -> Vec<MessageType> {
        unimplemented!()
    }

    fn run(&mut self) {
        unimplemented!();
    }
}
