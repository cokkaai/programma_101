use crate::message::Key;

pub struct KeyMap {
    pub character: char,
    pub key: Key,
}

impl KeyMap {
    pub fn new(character: char, key: Key) -> Self {
        Self {
            character,
            key
        }
    }
}

pub struct Mapping {
}

impl Mapping {
    pub fn standard() -> Vec<KeyMap> {
        vec!(KeyMap::new('+', Key::Add),
            KeyMap::new('c', Key::Clear),
            KeyMap::new(',', Key::Comma),
            KeyMap::new('m', Key::CopyM),
            KeyMap::new('a', Key::CopyToA),
            KeyMap::new('^', Key::DecimalUp),
            KeyMap::new('v', Key::DecimalDown),
            KeyMap::new('0', Key::Zero),
            KeyMap::new('1', Key::One),
            KeyMap::new('2', Key::Two),
            KeyMap::new('3', Key::Three),
            KeyMap::new('4', Key::Four),
            KeyMap::new('5', Key::Five),
            KeyMap::new('6', Key::Six),
            KeyMap::new('7', Key::Seven),
            KeyMap::new('8', Key::Eight),
            KeyMap::new('9', Key::Nine),
            KeyMap::new(':', Key::Divide),
            KeyMap::new('r', Key::GeneralReset),
            //KeyMap::new('k', Key::KeyboardRelease),
            KeyMap::new('x', Key::Multiply),
            KeyMap::new('n', Key::Negative),
            KeyMap::new('!', Key::Power),
            KeyMap::new('p', Key::PrintProgram),
            KeyMap::new('s', Key::RecordProgram),
            KeyMap::new('A', Key::A),
            KeyMap::new('B', Key::B),
            KeyMap::new('C', Key::C),
            KeyMap::new('D', Key::D),
            KeyMap::new('E', Key::E),
            KeyMap::new('F', Key::F),
            KeyMap::new('R', Key::R),
            KeyMap::new('*', Key::GeneralReset),
            KeyMap::new('V', Key::V),
            KeyMap::new('W', Key::W),
            KeyMap::new('Y', Key::Y),
            KeyMap::new('Z', Key::Z),
            KeyMap::new('/', Key::Split),
            KeyMap::new('S', Key::Start),
            KeyMap::new('-', Key::Subtract),
            KeyMap::new('w', Key::SwapA),
            KeyMap::new('t', Key::TapeAdvance),
        )
    }
}
