use crate::system::Message;

/// The trait defines common aspects of a keyboard device.
/// It defines:
/// - a function to get pressed key
/// - a function to get device's name
pub trait KeyboardProvider {
    fn get_name(&self) -> &str;

    /// Return an event describing the pressed key or None. The keyboard component calls this
    /// provider 60 times per seconds to provide a timely input and the poll function should
    /// not block. If it blocks, the pool function mmust guarantee that any input is returned
    /// as soon as possible in order to be responsive. The function returns a message.
    /// This generalization is necessary because the input device may implement a method to stop
    /// the emulator sending the Halt message.
    fn poll(&mut self) -> Option<Message>;
}

