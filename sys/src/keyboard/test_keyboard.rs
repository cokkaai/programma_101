use crate::message::*;
use crate::keyboard::KeyboardProvider;

/// Provider returning a sequence of predefined keys.
/// Used only for unit testing.
pub struct TestKeyboard {
    keys: Vec<Key>,
    pos: usize
}

impl TestKeyboard {
    pub fn new(keys: Vec<Key>) -> TestKeyboard {
        TestKeyboard {
            keys: keys,
            pos: 0
        }
    }
}

impl KeyboardProvider for TestKeyboard {
    fn get_name(&self) -> &str {
        "TestKeyboard"
    }

    fn poll(&mut self) -> Option<Message> {
        if self.pos < self.keys.len() {
            let key = self.keys[self.pos];
            self.pos += 1;
            Some(Message::KeyPressed(key))
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_keyboard_returns_configured_keys() {

        // Arrange
        
        let keys = vec!(Key::Start, Key::One, Key::Clear);
        let mut provider = TestKeyboard::new(keys);

        // Act and Assert
        
        assert_eq!(Some(Message::KeyPressed(Key::Start)), provider.poll());
        assert_eq!(Some(Message::KeyPressed(Key::One)), provider.poll());
        assert_eq!(Some(Message::KeyPressed(Key::Clear)), provider.poll());
        assert_eq!(None, provider.poll());
    }
}
