#[allow(dead_code)]
pub mod message;

#[allow(dead_code)]
pub mod system;

#[allow(dead_code)]
pub mod alu;

// #[allow(dead_code)]
pub mod keyboard;

#[allow(dead_code)]
pub mod printer;

#[allow(dead_code)]
pub mod storage;

#[allow(dead_code)]
pub mod halt;

#[allow(dead_code)]
pub mod status;

pub mod gui;
