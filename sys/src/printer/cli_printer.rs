use crate::printer::PrinterProvider;

pub struct CliPrinter;

impl CliPrinter {
    pub fn new() -> Self {
        Self
    }
}

impl Default for CliPrinter {
    fn default() -> Self {
        Self::new()
    }
}

impl PrinterProvider for CliPrinter {
    fn get_name(&self) -> &str {
        "CliPrinter"
    }

    fn print(&mut self, text: &str) {
        print!("PRN: {}", text);
    }

    fn print_line(&mut self, text: &str) {
        println!("PRN: {}", text);
    }
}
