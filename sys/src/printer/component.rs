use crate::message::*;
use crate::system::{Component, ComponentLink};
use crate::printer::PrinterProvider;
use std::sync::mpsc;

pub struct PrinterComponent<T: PrinterProvider> {
    provider: T,
    ready: bool,
    link: ComponentLink
}

impl<T: PrinterProvider> PrinterComponent<T> {
    pub fn new(provider: T, system_channel_sender: mpsc::Sender<Message>) -> Self {
        Self {
            provider,
            ready: false,
            link: ComponentLink::new(system_channel_sender)
        }
    }

    fn process_message(&mut self, message: Message) {
        log::debug!("Received message {:?}", message);

        match message {
            Message::SystemStatusChanged(SystemStatus::Ready) => self.ready = true,
            Message::SystemStatusChanged(_) => self.ready = false,
            Message::Print(text) => if self.ready {
                self.provider.print(&text);
            },
            Message::PrintLine(text) => if self.ready {
                self.provider.print_line(&text)
            },
            _ => ()
        }
    }
}

impl<T: PrinterProvider> Component for PrinterComponent<T> {
    fn get_name(&self) -> &str {
        self.provider.get_name()
    }

    fn listen_to(&self) -> Vec<MessageType> {
        vec!(MessageType::Print, MessageType::PrintLine, MessageType::SystemStatusChanged)
    }

    fn get_sender(&self) -> mpsc::Sender<Message> {
        self.link.component_sender.clone()
    }

    fn run(&mut self) {
        loop {
            match self.link.component_receiver.recv() {
                Ok(message) => if message == Message::Halt {
                    log::info!("{} received an halt message and it is hanging on", self.provider.get_name());
                    break;
                } else {
                    self.process_message(message);
                },
                Err(mpsc::RecvError) => log::error!("{} received an error reading the input channel and it is hanging on", self.provider.get_name())
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::printer::TestPrinter;
    use std::sync::{Arc, RwLock};
    use std::thread;

    fn create_printer() -> (PrinterComponent<TestPrinter>, Arc<RwLock<Vec<String>>>, mpsc::Receiver<Message>) {
        let (system_sender, system_receiver) = mpsc::channel();
        let (provider, logs) = TestPrinter::new();
        let component = PrinterComponent::new(provider, system_sender);
        (component, logs, system_receiver)
    }

    #[test]
    pub fn when_the_printer_component_receives_an_halt_message_then_it_stops() {
        // Arrange
        let (mut printer, _logs, _system) = create_printer();
        let sender = printer.link.component_sender.clone();

        // Act
        let handle = thread::spawn(move || {
            printer.run();
        });
        sender.send(Message::Halt).unwrap();

        // Assert
        let _ = handle.join();
        assert!(true, "the component should halt");
    }

    #[test]
    pub fn when_a_print_message_is_received_then_print_some_text() {

        // Arrange
        
        let (mut printer, logs, _system) = create_printer();
        let sender = printer.link.component_sender.clone();
        let handle = thread::spawn(move || {
            printer.run();
        });

        // Act

        sender.send(Message::SystemStatusChanged(SystemStatus::Ready)).unwrap();
        sender.send(Message::Print("a, ".to_string())).unwrap();
        sender.send(Message::Print("b, ".to_string())).unwrap();
        sender.send(Message::Print("c.".to_string())).unwrap();
        sender.send(Message::Halt).unwrap();

        // Assert
        
        let _ = handle.join();
        let a = logs.read().unwrap();
        assert_eq!(1, a.len(), "unexpected number of printed lines");
        assert_eq!("a, b, c.".to_string(), a[0], "unexpected text");
    }

    #[test]
    pub fn when_a_print_line_message_is_received_then_print_a_line_of_text() {
        
        // Arrange
        
        const TEXT: &str = "This is a new line.";
        let (mut printer, logs, _system) = create_printer();
        let sender = printer.link.component_sender.clone();

        // Act
        
        let handle = thread::spawn(move || {
            printer.run();
        });
        sender.send(Message::SystemStatusChanged(SystemStatus::Ready)).unwrap();
        sender.send(Message::PrintLine(TEXT.to_string())).unwrap();
        sender.send(Message::Halt).unwrap();

        // Assert

        let _ = handle.join();
        let a = logs.read().unwrap();
        assert_eq!(1, a.len(), "unexpected number of printed lines");
        assert_eq!(TEXT.to_string(), a[0], "unexpected text");
    }

    #[test]
    pub fn when_multiple_print_messages_are_received_then_print_them_in_order() {

        // Arrange
        
        let (mut printer, logs, _system) = create_printer();
        let sender = printer.link.component_sender.clone();
        let handle = thread::spawn(move || {
            printer.run();
        });

        // Act

        sender.send(Message::SystemStatusChanged(SystemStatus::Ready)).unwrap();
        
        // Line 0
        sender.send(Message::Print("a, ".to_string())).unwrap();
        sender.send(Message::Print("b, ".to_string())).unwrap();
        sender.send(Message::PrintLine("c.".to_string())).unwrap();
        
        // Line 1
        sender.send(Message::PrintLine("ddd".to_string())).unwrap();
        
        // Line 2
        sender.send(Message::Print("a, ".to_string())).unwrap();
        sender.send(Message::PrintLine("ddd".to_string())).unwrap();
        
        // Line 3
        sender.send(Message::PrintLine("ddd".to_string())).unwrap();

        sender.send(Message::Halt).unwrap();

        // Assert
        let _ = handle.join();
        let a = logs.read().unwrap();
        assert_eq!(4, a.len(), "unexpected number of printed lines");
        assert_eq!("a, b, c.".to_string(), a[0], "unexpected text at line 0");
        assert_eq!("ddd".to_string(), a[1], "unexpected text at line 0");
        assert_eq!("a, ddd".to_string(), a[2], "unexpected text at line 0");
        assert_eq!("ddd".to_string(), a[3], "unexpected text at line 0");
    }
}

