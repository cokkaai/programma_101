/// The trait defines common aspects of a printer device.
/// It defines:
/// - two function for writing text to whatever output media
/// - a function to get printer's name
pub trait PrinterProvider {
    fn get_name(&self) -> &str;

    /// Prints the text starting from the printer head position.
    fn print(&mut self, text: &str);

    /// Prints the text at printer head position
    /// and start a new line.
    fn print_line(&mut self, text: &str);
}
