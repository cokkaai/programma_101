use std::sync::RwLock;
use std::sync::Arc;
use crate::printer::PrinterProvider;

enum WriteMode {
    /// Append new data to logs
    Append,

    /// Concatenate new data to last log
    Update,
}

/// The test printer features a log
/// which keeps all string wrote.
pub struct TestPrinter {
    logs: Arc<RwLock<Vec<String>>>,
    write_mode: WriteMode,
}

impl TestPrinter{
    /// Creates a new test printer logging data
    /// to a self-created string vector.
    pub fn new() -> (Self, Arc<RwLock<Vec<String>>>) {
        let logs = Arc::new(RwLock::new(Vec::new()));

        let printer = Self {
            logs: logs.clone(),
            write_mode: WriteMode::Append,
        };

        (printer, logs)
    }

    /// Creates a new test printer logging data
    /// to the provided string vector.
    pub fn new_with_logs(logs: Arc<RwLock<Vec<String>>>) -> Self {
        Self {
            logs,
            write_mode: WriteMode::Append,
        }
    }

    /// Store a text in the log. Depending on how write mode
    /// is set *before* invoking the fn, text is handled differently.WriteMode
    /// In Append mode, text is just a new element of the log vector,
    /// In Update mode, text is appended to the last text of the vector, if any;
    /// should the vector be empty, text is appended.
    fn log(&mut self, text: &str) {
        let mut logs = self.logs.write().unwrap();
        
        match self.write_mode {
            WriteMode::Append => logs.push(text.to_string()),
            WriteMode::Update => if let Some(last) = logs.last_mut() {
                *last += text;
            } else {
                logs.push(text.to_string());
            }
        }
    }
}

impl PrinterProvider for TestPrinter{
    fn get_name(&self) -> &str {
        "TestPrinter"
    }

    fn print(&mut self, text: &str) {
        self.log(text);
        self.write_mode = WriteMode::Update;
    }

    fn print_line(&mut self, text: &str) {
        self.log(text);
        self.write_mode = WriteMode::Append;
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    const SOME_TEXT: &str = "some text 01";

    #[test]
    pub fn printing_some_text_on_an_empty_log_adds_a_new_line() {
        let (mut printer, log) = TestPrinter::new();

        printer.print(SOME_TEXT);

        let log = log.read().unwrap();
        assert_eq!(1, log.len());
        assert_eq!(SOME_TEXT, log.first().unwrap());
    }
    

    #[test]
    pub fn printing_a_new_line_adds_a_new_line() {
        let (mut printer, log) = TestPrinter::new();

        printer.print_line(SOME_TEXT);

        {
            let log = log.read().unwrap();
            assert_eq!(1, log.len());
            assert_eq!(SOME_TEXT, log.first().unwrap());
        }

        printer.print_line(SOME_TEXT);

        {
            let log = log.read().unwrap();
            assert_eq!(2, log.len());
            assert_eq!(SOME_TEXT, log.first().unwrap());
        }
    }
    
    #[test]
    pub fn printing_more_text_on_the_same_line_updates_last_text() {
        let (mut printer, log) = TestPrinter::new();

        printer.print(SOME_TEXT);
        printer.print(SOME_TEXT);

        {
            let log = log.read().unwrap();

            assert_eq!(1, log.len());

            let mut expected = String::new();
            expected.push_str(SOME_TEXT);
            expected.push_str(SOME_TEXT);

            assert_eq!(expected, *log.first().unwrap());
        }
    }
}
