use std::path;
use std::fs;
use std::fmt;

#[derive(Debug, PartialEq, Eq)]
pub enum CardError {
    NoCardInserted,
    ReadError(String),
    WriteError(String)
}

impl fmt::Display for CardError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> { 
        let msg = match self {
            CardError::NoCardInserted => "No card inserted".to_string(),
            CardError::ReadError(error) => format!("Cannot read card: {}", error),
            CardError::WriteError(error) => format!("Cannot write card: {}", error)
        };

        write!(f, "{}", msg)
    }
}

pub enum Card {
    MemoryCard { text: String },
    FileCard { path: String }
}

impl Card {
    pub fn read(&self) -> Result<String, CardError> {
        match self {
            Card::MemoryCard { text } => Ok(text.to_string()),
            Card::FileCard { path } => Self::read_file(path)
        }
    }
    
    pub fn write(&mut self, new_text: &str) -> Result<(), CardError> {
        match self {
            Card::MemoryCard { text } => {
                *text = new_text.to_string();
                Ok(())
            },
            Card::FileCard { path } => Self::write_file(path, new_text)
        }
    }

    fn read_file(path: &str) -> Result<String, CardError> {
        if path::Path::new(path).exists() {
            match fs::read_to_string(path) {
                Ok(text) => Ok(text),
                Err(e) => {
                    let msg = format!("Cannot read file \"{}\": {}", path, e);
                    Err(CardError::ReadError(msg))
                }
            }
        } else {
            let msg = format!("File \"{}\" does not exists", path);
            Err(CardError::ReadError(msg))
        }
    }
    
    fn write_file(path: &str, text: &str) -> Result<(), CardError> {
        let data = text.as_bytes();

        match fs::write(path, data) {
            Ok(_) => Ok(()),
            Err(e) => {
                let msg = format!("Cannot write to file \"{}\"; {}", path, e);
                Err(CardError::ReadError(msg))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn when_a_memory_card_is_read_then_return_its_content() {
        // Arrange
        const CONTENT: &str = "This is is card content.";
        let card = Card::MemoryCard { text: CONTENT.to_string() };

        // Act
        let result = card.read();
        
        // Assert
        match result {
            Ok(text) => assert_eq!(CONTENT.to_string(), text, "Card does not containt the expected text"),
            _ => assert!(false, "Error reading the card")
        }
    }

    #[test]
    pub fn when_a_memory_card_is_written_then_return_its_content() {
        // Arrange
        const CONTENT: &str = "Updated content.";
        let mut card = Card::MemoryCard { text: "This content should not be found at the end of test.".to_string() };

        // Act
        let result = card.write(CONTENT);
        
        // Assert
        assert!(result.is_ok(), "Error writing the card");

        match card.read() {
            Ok(text) => assert_eq!(CONTENT.to_string(), text, "Card does not containt the expected text"),
            _ => assert!(false, "Error reading the card")
        }
    }

    #[test]
    pub fn when_reading_a_missing_file_then_return_an_error() {
        // Arrange
        const EXPECTED_ERROR: &str = "File \"missing_file.txt\" does not exists";
        let card = Card::FileCard { path: "missing_file.txt".to_string() };

        // Act
        let result = card.read();
        
        // Assert
        match result {
            Err(e) => assert_eq!(CardError::ReadError(EXPECTED_ERROR.to_string()), e),
            _ => assert!(false, "Reading the card should have failed")
        }
    }

    #[test]
    pub fn when_reading_a_file_then_return_its_content() {
        const CONTENT: &str = "This is is card content.";
        const PATH: &str = "file_card__when_reading_a_file_then_return_its_content.txt";

        // Arrange

        let _ = std::fs::remove_file(PATH);
        let mut card = Card::FileCard { path: PATH.to_string() };
        let _ = card.write(CONTENT);

        // Act

        let result = card.read();

        // Assert

        match result {
            Ok(content) => assert_eq!(CONTENT.to_string(), content, "Unexpected file content"),
            Err(e) => panic!("Error reading card: {}", e)
        }

        // Clean

        let _ = std::fs::remove_file(PATH);
    }

    #[test]
    pub fn when_writing_a_file_then_return_an_ok_on_success() {
        const CONTENT: &str = "This is is card content.";
        const PATH: &str = "file_card__when_writing_a_file_then_return_an_ok_on_success.txt";

        // Arrange

        let _ = std::fs::remove_file(PATH);
        let mut card = Card::FileCard { path: PATH.to_string() };

        // Act

        let result = card.write(CONTENT);

        // Assert

        assert_eq!(Ok(()), result, "Error writing to card");

        match card.read() {
            Ok(content) => assert_eq!(CONTENT.to_string(), content, "Unexpected file content"),
            Err(e) => panic!("Error reading card: {}", e)
        }

        // Clean

        let _ = std::fs::remove_file(PATH);
    }
}
