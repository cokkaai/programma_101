use super::card::Card;
use crate::message;
use crate::system::{Component, ComponentLink};
use std::sync::mpsc;

pub struct StorageComponent {
    card: Option<Card>,
    link: ComponentLink,
}

impl StorageComponent {
    pub fn new(system: mpsc::Sender<message::Message>) -> Self {
        Self {
            card: None,
            link: ComponentLink::new(system)
        }
    }

    fn create_card(card: message::Card) -> Card {
        match card {
            message::Card::MemoryCard { text } => Card::MemoryCard { text },
            message::Card::FileCard { path } => Card::FileCard { path }
        }
    }

    fn send(&self, msg: message::Message) {
        self.link.system.send(msg).unwrap();
    }

    fn read_card(&self) {
        match &self.card {
            Some(card) => {
                match card.read() {
                    Ok(text) => self.send(message::Message::TextRead(text)),
                    Err(e) => log::error!("{}", e)
                }
            },
            _ => log::error!("No card in reader")
        }
    }

    fn write_card(&mut self, new_text: &str) {
        if self.card.is_none() {
            log::error!("No card in reader");
        } else if let Err(e) = self.card.as_mut().unwrap().write(new_text) {
            log::error!("{}", e);
        }
    }
}

impl Component for StorageComponent {
    fn get_name(&self) -> &str {
        "StorageComponent"
    }

    fn listen_to(&self) -> Vec<message::MessageType> {
        vec!(message::MessageType::CardInserted, 
             message::MessageType::CardRemoved, 
             message::MessageType::StoreTextOnCard, 
             message::MessageType::ReadTextFromCard)
    }

    fn get_sender(&self) -> mpsc::Sender<message::Message> {
        self.link.component_sender.clone()
    }

    fn run(&mut self) {
        loop {
            match self.link.component_receiver.recv() {
                Ok(message::Message::CardInserted(card)) => {
                    let new_card = Self::create_card(card);
                    self.card = Some(new_card);
                },
                Ok(message::Message::CardRemoved) => self.card = None,
                Ok(message::Message::ReadTextFromCard) => self.read_card(),
                Ok(message::Message::StoreTextOnCard(ref text)) => self.write_card(text),
                Ok(message::Message::Halt) => return,
                _ => (),
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::message::*;
    use crate::storage::StorageComponent;
    use crate::system::Component;
    use std::sync::mpsc;
    use std::thread;
    use std::time::Duration;

    fn create_empty_storage() -> (StorageComponent, mpsc::Receiver<Message>) {
        let (system_sender, system_receiver) = mpsc::channel();
        (StorageComponent::new(system_sender), system_receiver)
    }

    #[test]
    pub fn when_the_storage_component_receives_an_halt_message_then_it_stops() {
        // Arrange
        let (mut storage, _system) = create_empty_storage();
        let sender = storage.link.component_sender.clone();

        // Act
        let handle = thread::spawn(move || storage.run());
        sender.send(Message::Halt).unwrap();

        // Assert
        let _ =handle.join();
        assert!(true, "the component should halt");
    }

    #[test]
    pub fn when_a_card_is_inserted_then_storage_takes_ownership() {
        // Arrange
        const TEXT: &str = "this is a test";
        let (mut storage, _system) = create_empty_storage();
        let sender = storage.link.component_sender.clone();

        // Act
        let handle = thread::spawn(move || {
            storage.run();
            storage.card
        });

        sender.send(Message::CardInserted(Card::new_memory_card(TEXT))).unwrap();
        sender.send(Message::Halt).unwrap();

        // Assert
        let result = handle.join();

        match result {
            Ok(Some(super::Card::MemoryCard { text} )) => assert_eq!(TEXT.to_string(), text, "Unexpected card content"),
            _ => panic!("Unexpected card status")
        }
    }

    #[test]
    pub fn when_a_card_is_read_then_storage_posts_its_content_on_the_system_bus() {
        // Arrange
        const TEXT: &str = "this is a test";
        let (mut storage, system_receiver) = create_empty_storage();
        let sender = storage.link.component_sender.clone();

        // Act
        let handle = thread::spawn(move || {
            storage.run();
        });

        sender.send(Message::CardInserted(Card::new_memory_card(TEXT))).unwrap();
        sender.send(Message::ReadTextFromCard).unwrap();
        sender.send(Message::Halt).unwrap();

        // Assert
        let _ = handle.join();

        match system_receiver.recv_timeout(Duration::from_secs(1)).unwrap() {
            Message::TextRead(read) => assert_eq!(TEXT.to_string(), read),
            _=> assert!(false, "the component did not send the text read message")
        }
    }

    #[test]
    pub fn when_a_card_is_wrote_then_storage_updates_its_contents() {
        // Arrange
        let (mut storage, _system_receiver) = create_empty_storage();
        let sender = storage.link.component_sender.clone();
        const TEXT: &str = "this is a test";
        const TEXT2: &str = "bla bla bla";

        // Act
        let handle = thread::spawn(move || {
            storage.run();
            storage.card
        });

        sender.send(Message::CardInserted(Card::new_memory_card(TEXT))).unwrap();
        sender.send(Message::StoreTextOnCard(TEXT2.to_string())).unwrap();
        sender.send(Message::Halt).unwrap();
        
        let result = handle.join();

        // Assert
        match result {
            Ok(Some(super::Card::MemoryCard { text } )) => assert_eq!(TEXT2.to_string(), text, "Unexpected card content"),
            _ => panic!("Unexpected card status")
        }
    }
}
