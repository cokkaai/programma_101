use std::sync::mpsc;
use crate::message::*;

pub trait Component {
    /// Returns the component name.
    fn get_name(&self) -> &str;

    /// Returns the sender to push events to the component.
    /// Channel where to push the event
    fn get_sender(&self) -> mpsc::Sender<Message>;

    /// Enumerates the events relevant to the component
    /// and the sender to push messages
    /// List of event types
    /// Ex: Message::CardRemoved as u8
    fn listen_to(&self) -> Vec<MessageType>;

    /// Run the component in its own thread.
    /// The component is responsible for handling Message::Halt
    /// and exit cleanly when recaived.
    fn run(&mut self);
}

pub struct ComponentLink {
    /// The sender to post messages on the system bus
    pub system: mpsc::Sender<Message>,

    /// The sender of the component channel
    pub component_sender: mpsc::Sender<Message>,

    /// The receiver of the component channel
    pub component_receiver: mpsc::Receiver<Message>
}

impl ComponentLink {
    pub fn new(system_channel_sender: mpsc::Sender<Message>) -> Self {
        let (s, r) = mpsc::channel();
        
        Self {
            system: system_channel_sender,
            component_sender: s,
            component_receiver: r
        }
    }

}

