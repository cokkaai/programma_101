/*
#[cfg(test)]
mod tests {
    use p101_sys::system::*;
    use p101_sys::message::*;
    use p101_sys::printer::TestPrinter;

    /// The print event producer generate some print events
    /// for testing purpose.
    struct PrintMessageProducer {
        status: u8,
    }

    impl PrintMessageProducer {
        pub const FIRST_LOG: &'static str = "new line!";
        pub const SECOND_LOG: &'static str = "start";
        pub const THIRD_LOG: &'static str = " and end.";

        fn new() -> Self {
            Self {
                status: 0,
            }
        }
    }

    impl Component for PrintMessageProducer {
        fn get_name(&self) -> &str {
            "PrintMessageProducer"
        }

        fn listen_to(&self) -> Vec<any::TypeId> {
            unimplemented();
        }
    
        fn run(&mut self);
            let mut messages = Vec::new();

            match self.status {
                0 => messages.push(Message::PrintLine(Self::FIRST_LOG.to_string())),
                1 => messages.push(Message::Print(Self::SECOND_LOG.to_string())),
                2 => messages.push(Message::PrintLine(Self::THIRD_LOG.to_string())),
                _ => (),
            }

            self.status += 1;
        }
    }

    /*
    /// This integration test verifies that:
    /// - the system is able to route print messages to consumers
    /// - and that the printer manages these messages
    #[test]
    pub fn when_system_process_print_messages_some_texts_are_wrote_to_the_output_channel() {
        let (_sender, receiver) = std::sync::mpsc::channel::<Message>();
        let mut sys = System::new(receiver);
        let (printer, logs) = TestPrinter::new();

        sys.register_producer(Box::new(PrintMessageProducer::new()));
        sys.register_consumer(Box::new(printer));

        sys.run();

        {
            let logs = logs.read().unwrap();

            assert_eq!(1, logs.len());
            assert_eq!(PrintMessageProducer::FIRST_LOG, logs[0]);
        }

        sys.run();

        {
            let logs = logs.read().unwrap();

            assert_eq!(2, logs.len());
            assert_eq!(PrintMessageProducer::SECOND_LOG, logs[1]);
        }

        sys.run();

        {
            let logs = logs.read().unwrap();

            assert_eq!(2, logs.len());
            assert_eq!("start and end.", logs[1]);
        }
    }
    */

*/
